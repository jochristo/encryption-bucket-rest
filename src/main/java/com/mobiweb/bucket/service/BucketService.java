package com.mobiweb.bucket.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.common.Utilities;
import static com.mobiweb.bucket.common.Utilities.createPathFromName;
import static com.mobiweb.bucket.common.Utilities.deletePath;
import static com.mobiweb.bucket.common.Utilities.firstThreeChars;
import static com.mobiweb.bucket.common.Utilities.isEmpty;
import com.mobiweb.bucket.core.BaseBucketFile;
import com.mobiweb.bucket.exceptions.BucketExistsException;
import com.mobiweb.bucket.exceptions.BucketStorageException;
import com.mobiweb.bucket.exceptions.UnknownBucketException;
import com.mobiweb.bucket.cryptography.Cryptographic;
import com.mobiweb.bucket.core.Bucket;
import com.mobiweb.bucket.core.BucketFileItem;
import com.mobiweb.bucket.core.Decrypted;
import com.mobiweb.bucket.core.Encrypted;
import com.mobiweb.bucket.core.IBucketFactory;
import com.mobiweb.bucket.core.IBucketFileFactory;
import com.mobiweb.bucket.core.PropertiesBucketFile;
import com.mobiweb.bucket.core.RegularBucketFile;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * Contains methods and properties to handle operations 
 * on {@link com.mobiweb.bucket.core.Bucket} instances.
 * @author ic
 */
@Singleton
@LocalBean
public class BucketService implements IBucketFileFactory<Bucket,BaseBucketFile>, IBucketFactory<Bucket>
{    
    private static final Logger LOGGER = Logger.getLogger(BucketService.class.getName());    
    //private static final Logger Logger = LoggerFactory.getLogger(BucketService.class.getName());    
    //private final Handler handlerObj;    
    private static final String CATALINA_BASE_DIR = System.getProperty("catalina.base");
    private static final String PROPERTIES_FILE_EXTENSION = ".properties";    
        
    @Resource(name="bucketStoragePath")
    private String storageFolderPath;
    
    /**
     * Initializes a new instance of {@link BucketService} class with default properties.
     * @throws EncryptionException 
     */
    public BucketService() throws EncryptionException
    {
        // set Console handler properties
        //handlerObj = new ConsoleHandler();
        //handlerObj.setLevel(Level.ALL);        
        //LOGGER.addHandler(handlerObj);
        //LOGGER.setLevel(Level.ALL);
        //LOGGER.setUseParentHandlers(false);
    }    

    @Override
    public RegularBucketFile createFile(Bucket bucket, BucketFileItem item, SecretKey secretKey) throws BucketStorageException, EncryptionException
    {
        // Do some checking    
        if(Utilities.isEmpty(item))
        {
            throw new BucketStorageException("Bucket file item is empty or null");           
        }     
        
        if(Utilities.isEmpty(secretKey))
        {
            throw new BucketStorageException("Secret Key is empty or null");
        }   
        
        if(Utilities.isEmpty(bucket))
        {
            throw new BucketStorageException("Bucket is null");
        }
        
        
        LOGGER.log(Level.INFO, "Creating bucket file :{0}", item.getFilename()); 
        RegularBucketFile regularBucketFile = new RegularBucketFile(item,bucket).encrypted(new Cryptographic(secretKey));                
        bucket.getBucketFiles().add(regularBucketFile);
        
        LOGGER.log(Level.INFO, "Added bucket file to bucket: {0}", bucket.getRelativePath()); 
        updatePropertiesFile(bucket, secretKey);
        
        LOGGER.log(Level.INFO, "Updated bucket properties file: {0}", bucket.getPropertiesFile()); 
        return regularBucketFile;        
    }

    @Override
    public RegularBucketFile updateFile(Bucket bucket, BucketFileItem item, SecretKey secretKey) throws BucketStorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    

    @Override
    public boolean deleteFile(Bucket bucket, String filename, SecretKey secretKey) throws BucketStorageException, IOException
    {
        // Do some checking    
        if(Utilities.isEmpty(filename))
        {
            throw new BucketStorageException("File name is empty or null");
        }
        if(Utilities.isEmpty(secretKey))
        {
            throw new BucketStorageException("Secret Key is empty or null");
        }
        if(Utilities.isEmpty(bucket))
        {
            throw new BucketStorageException("Bucket is null");
        }         
        
        if(!exists(bucket, filename))
        {
           throw new BucketStorageException("Unknown bucket item: " + filename); 
        }
        
        LOGGER.log(Level.INFO, "Bucket item found :{0}", filename);                  
        String path = CATALINA_BASE_DIR.concat(storageFolderPath) + bucket.getRelativePath() + File.separator + filename;        
        File file = new File(path).getAbsoluteFile();             
        String abs = file.getAbsolutePath();        
        return deletePath(abs);    
    }

    
    @Override
    public Bucket create(String bucketName, SecretKey secretKey) throws BucketStorageException, BucketExistsException, IOException
    {
        // Do some checking
        if(isEmpty(CATALINA_BASE_DIR.concat(storageFolderPath)))
        {
            throw new BucketStorageException("Bucket storage folder is invalid or does not exist");
        }         
        if(exists(bucketName))
        {
            throw new BucketExistsException("Bucket name exists: " + bucketName);
        }    
                
        // Build and create new bucket
        String bucketStoragePath = CATALINA_BASE_DIR.concat(storageFolderPath);    
        String bucketRelativePath = createPathFromName(bucketName).concat(bucketName);         
        File file = new File(bucketStoragePath.concat(bucketRelativePath));        
        boolean mkdirs = file.mkdirs();           
        if(mkdirs)
        {   
            try
            {
                // Create bucket properties file
                Bucket bucket = new Bucket(bucketName, new File(bucketStoragePath).getAbsolutePath());  
                createPropertiesFile(bucket, secretKey);                
                LOGGER.log(Level.INFO, "Bucket created : {0}", bucketName);   
                //Logger.info("Bucket created :{0}", bucketName);
                return bucket;
            }
            catch(BucketStorageException ex)
            {
                // delete path
                Utilities.deletePath(file.getAbsolutePath());
                throw new BucketStorageException("Bucket was not created"); 
            }
        }
        else
        {
           throw new BucketStorageException("Bucket was not created"); 
        }
    }

    @Override
    public boolean delete(String bucketName, SecretKey secretKey) throws BucketStorageException, IOException, UnknownBucketException
    {
        if(isEmpty(bucketName))
        {
            throw new BucketStorageException("Bucket name is empty");
        }  
        //load bucket
        Bucket bucket = load(bucketName, secretKey);   
        if(isEmpty(bucket))
        {
            throw new BucketStorageException("Bucket is null");
        }
        String bucketStoragePath = CATALINA_BASE_DIR.concat(storageFolderPath);             
        File file = new File(bucketStoragePath.concat(createPathFromName(bucket.getName()).concat(bucket.getName()))); 
        String abs = file.getAbsolutePath();
        deletePath(abs);        
        return true;
    }
    
    /**
     * Checks in server file system for existing bucket name (folder) with the same name.
     * @param bucketName
     * @return 
     */
    @Override
    public boolean exists(String bucketName)
    {
        String extracted = firstThreeChars(bucketName);
        char[] chars = extracted.toCharArray();
        File f = new File(CATALINA_BASE_DIR.concat(storageFolderPath)).getAbsoluteFile();        
        String absolutePath = f.getAbsolutePath(); 
        String bucketPath = Utilities.toString(chars[0]).concat(File.separator) + Utilities.toString(chars[1]).concat(File.separator) + Utilities.toString(chars[2]).concat(File.separator);
        File file = new File(absolutePath.concat(File.separator).concat(bucketPath).concat(File.separator).concat(bucketName));
        return file.exists();            
    }     
    
    /**
     * Determines if given bucket file item exists.
     * @param bucket
     * @param filename
     * @return 
     */
    @Override
    public boolean exists(Bucket bucket, String filename)
    {     
        String storagePath = CATALINA_BASE_DIR.concat(storageFolderPath) + bucket.getRelativePath() + File.separator + filename;        
        File file = new File(storagePath).getAbsoluteFile();                
        return file.exists();            
    }     

    @Override
    public Bucket load(String bucketName, SecretKey secretKey) throws BucketStorageException, UnknownBucketException, IOException
    {

            // Do some checking
            if(!exists(bucketName))
            {
                throw new UnknownBucketException("Unknown bucket name: " + bucketName);
            }     
            if(isEmpty(CATALINA_BASE_DIR.concat(storageFolderPath)))
            {
                throw new BucketStorageException("Bucket storage folder is invalid or it does not exist");
            }         

            // Read or create properties file and load bucket
            String bucketStoragePath = CATALINA_BASE_DIR.concat(storageFolderPath);     
            String bucketRelativePath = createPathFromName(bucketName).concat(bucketName);         
            String propertiesFilePath = bucketRelativePath.concat(File.separator).concat(bucketName.concat(PROPERTIES_FILE_EXTENSION));
            File propertiesFile = new File(bucketStoragePath.concat(propertiesFilePath));
            Bucket bucket = new Bucket(bucketName, new File(bucketStoragePath).getAbsolutePath());
            if(propertiesFile.exists())
            {
                // Load properties file and bucket            
                return getBucketFromPropertiesFile(bucket, propertiesFile, secretKey);
            }
            else
            {
                // create properties file walking through bucket folder
                boolean isPropertiesFileCreated = writePropertiesFile(bucket, secretKey);
                if(isPropertiesFileCreated){
                    return getBucketFromPropertiesFile(bucket, propertiesFile, secretKey);
                }
            }
            return bucket;
    }
    
    @Override
    public Bucket getBucketFromPropertiesFile(Bucket bucket, File file, SecretKey secretKey) throws BucketStorageException
    {     
        try
        {
            Decrypted decrypted = new Decrypted(file.getAbsolutePath());           
            byte[] data = decrypted.transformed(new Cryptographic(), secretKey);                     
            String json = new String(data,"UTF-8");
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();         
            PropertiesBucketFile propertiesBucketFile = gson.fromJson(json, PropertiesBucketFile.class);
            propertiesBucketFile.getContainedBucketFiles().forEach((item)->
            {
                bucket.getBucketFiles().add(item);
            });
            return bucket;
        }
        catch(JsonParseException | EncryptionException | IOException ex)
        {
            throw new BucketStorageException(ex.getMessage(), ex);
        }
    }    
    
    @Override
    public void createPropertiesFile(Bucket bucket, String storagePath, SecretKey secretKey) throws BucketStorageException
    {
        try
        {
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();                                    
            PropertiesBucketFile pbf = new PropertiesBucketFile(bucket);            
            String json = gson.toJson(pbf, PropertiesBucketFile.class);            
            Encrypted encrypted = new Encrypted(json.getBytes("UTF-8"), new File(bucket.getAbsolutePath().concat(bucket.getPropertiesFile())).getAbsolutePath());
            byte[] data = encrypted.transformed(new Cryptographic(), secretKey);        
            Cryptographic.write(data, encrypted.getAbsoluteFilePath(), false);            
            LOGGER.log(Level.INFO, "Bucket properties file created : {0}", pbf.getFilename());  
            //Logger.info("Bucket properties file created : {0}", pbf.getFilename());
        }
        catch(EncryptionException | IOException ex)
        {
            throw new BucketStorageException(ex.getMessage(), ex);
        }
    }    

    @Override
    public PropertiesBucketFile createPropertiesFile(Bucket bucket,SecretKey secretKey) throws BucketStorageException
    {
        try
        {            
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();                                    
            PropertiesBucketFile pbf = new PropertiesBucketFile(bucket);            
            String json = gson.toJson(pbf, PropertiesBucketFile.class);            
            String absoluteFilePath = bucket.getAbsolutePath().concat(File.separator).concat(bucket.getPropertiesFile());
            Encrypted encrypted = new Encrypted(json.getBytes("UTF-8"), new File(absoluteFilePath).getAbsolutePath());
            byte[] data = encrypted.transformed(new Cryptographic(), secretKey);        
            Cryptographic.write(data, encrypted.getAbsoluteFilePath(), false);
            LOGGER.log(Level.INFO, "Bucket properties file created : {0}", pbf.getFilename());            
            return pbf;
        }
        catch(EncryptionException | IOException ex)
        {
            throw new BucketStorageException(ex.getMessage(), ex);
        }
    }

    @Override
    public PropertiesBucketFile getPropertiesFile(Bucket bucket, SecretKey secretKey) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PropertiesBucketFile updatePropertiesFile(Bucket bucket, SecretKey secretKey) throws BucketStorageException
    {
        try
        {
            // Load properties file and bucket         
            String filepath = bucket.getAbsolutePath() + File.separator + bucket.getPropertiesFile();
            File file = new File(filepath);      
            if(!file.exists())
            {
                writePropertiesFile(bucket,secretKey);
            }
   
            Decrypted decrypted = new Decrypted(file.getAbsolutePath());     
            byte[] data = decrypted.transformed(new Cryptographic(), secretKey);                     
            String json = new String(data,"UTF-8"); // for UTF-8 encoding
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();                
            PropertiesBucketFile propertiesBucketFile = gson.fromJson(json, PropertiesBucketFile.class);                        
            bucket.getBucketFiles().forEach((item)-> {
                propertiesBucketFile.getContainedBucketFiles().add((RegularBucketFile) item);
            });
            
            json = gson.toJson(propertiesBucketFile, PropertiesBucketFile.class);            
            Encrypted encrypted = new Encrypted(json.getBytes("UTF-8"), file.getAbsolutePath());
            byte[] encryptedData = encrypted.transformed(new Cryptographic(), secretKey);        
            Cryptographic.write(encryptedData, encrypted.getAbsoluteFilePath(), false);
            return propertiesBucketFile;
        }
        catch(JsonParseException | EncryptionException | IOException ex)
        {
            throw new BucketStorageException(ex.getMessage(), ex);
        }
    }

    @Override
    public boolean writePropertiesFile(Bucket bucket, SecretKey secretKey) throws BucketStorageException, IOException
    {
        String filepath = bucket.getAbsolutePath() + File.separator + bucket.getPropertiesFile();
        File file = new File(filepath);
        if(!file.exists())
        {            
            // use NIO to walk through tree
            Path path = Paths.get(bucket.getAbsolutePath());
            Files.walkFileTree(path, new FileVisitor<Path>()
            {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
                {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
                {                       
                    RegularBucketFile regularBucketFile = new RegularBucketFile(file.getFileName().toString(),bucket, true).append(attrs);
                    bucket.getBucketFiles().add(regularBucketFile);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException
                {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException
                {                    
                    return FileVisitResult.CONTINUE;
                }
            });  
            // create and update bucket properties file
            createPropertiesFile(bucket, secretKey);            
            updatePropertiesFile(bucket, secretKey);
        }
        return false;
    }

    @Override
    public RegularBucketFile getBucketFile(Bucket bucket, String filename, SecretKey secretKey) throws BucketStorageException
    {   
        try
        {
            if(!exists(bucket, filename))
            {
               throw new BucketStorageException("Unknown bucket item: " + filename); 
            }
            LOGGER.log(Level.INFO, "Bucket item found :{0}", filename);                  
            //Logger.info("Bucket item found :{0}", filename);
            return new RegularBucketFile(filename, bucket).decrypted(new Cryptographic(secretKey));
        }
        catch(EncryptionException | BucketStorageException | IOException ex)
        {
            throw new BucketStorageException(ex.getMessage(), ex);
        }
    }
    
}
