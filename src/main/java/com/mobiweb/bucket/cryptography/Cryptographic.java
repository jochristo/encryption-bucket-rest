package com.mobiweb.bucket.cryptography;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.io.IOUtils;

/**
 * Provides cryptographic methods using PBE with HMAC-SHA1 and AES algorithms.
 * @author ic
 */
public class Cryptographic
{
    private Cipher cipher;
    private int passwordLength;
    private int ivSeedLength;       
    private int saltLength;
    private static final String AES_WITH_PBE_TRASNFORMATION = "AES/CBC/PKCS5Padding";
    private static final String SECRET_KEY_SPEC_AES = "AES";
    private static final String SECRET_KEY_FACTORY_PBKDF2_HMACSHA1 = "PBKDF2WithHmacSHA1";    
    private static final int HASH_ITERATIONS = 10000;      
    private SecretKey secretKey;
    private final static Logger LOGGER = Logger.getLogger(Cryptographic.class.getName());  
    private static byte[] salt = "goddamnitshit1378,;_+#$".getBytes();
        
    /**
     * Initializes a new instance of {@link Cryptographic} class with default properties.
     * @throws EncryptionException 
     */    
    public Cryptographic() throws EncryptionException
    {
        this(16, 16, 16);
    }   
    
    /**
     * Initializes a new instance of {@link Cryptographic} class with given SecretKey object.
     * @param secretKey
     * @throws EncryptionException 
     */
    public Cryptographic(SecretKey secretKey) throws EncryptionException
    {
        this(16, 16, 16);
        this.secretKey = secretKey;
    }    
    
    /**
     * Instantiates a new object of Cryptographic class with parameters and random password length.
     * @param passwordLength
     * @param saltLength
     * @param ivSeedLength 
     * @throws com.mobiweb.bucket.cryptography.EncryptionException 
     */
    public Cryptographic(int passwordLength, int saltLength, int ivSeedLength) throws EncryptionException
    { 
        try
        {
            this.passwordLength = passwordLength;
            this.ivSeedLength = ivSeedLength;
            this.saltLength = saltLength;
            cipher = Cipher.getInstance(AES_WITH_PBE_TRASNFORMATION);
        }
        catch (NoSuchAlgorithmException | NoSuchPaddingException ex)
        {
            throw new EncryptionException(ex.getMessage(), ex);
        }
    }      

    public Cipher getCipher() {
        return cipher;
    }

    public void setCipher(Cipher cipher) {
        this.cipher = cipher;
    }

    public SecretKey getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(SecretKey secretKey) {
        this.secretKey = secretKey;
    }
    
    /**
     * Encrypts and ciphers given bytes with supplied SecretKey.
     * @param data
     * @param secretKey
     * @return byte[] Encrypted bytes.
     * @throws com.mobiweb.bucket.cryptography.EncryptionException
     */
    public byte[] encrypt(byte[] data, SecretKey secretKey) throws EncryptionException
    {
        try
        {
            this.secretKey = secretKey;
            // Create Initialization vector params
            SecureRandom secureRandom = new SecureRandom(); 
            byte[] seed = secureRandom.generateSeed(ivSeedLength);
            AlgorithmParameterSpec algorithmParameterSpec = new IvParameterSpec(seed);                      

            // Init cipher and encrypt data
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, algorithmParameterSpec);
            byte[] encryptedBytes = cipher.doFinal(data);

            // Mix up encrypted bytes and length with seed length
            byte[] bytesToEncode = new byte[seed.length + encryptedBytes.length];
            System.arraycopy(seed, 0, bytesToEncode, 0, seed.length);
            System.arraycopy(encryptedBytes, 0, bytesToEncode, seed.length, encryptedBytes.length);                 
            return bytesToEncode;        
        }
        catch(InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException ex)
        {
            throw new EncryptionException(ex.getMessage(), ex);
        } 
    }     
  
    /**
     * Decrypts given bytes using supplied SecretKey.
     * @param decrypted
     * @param secretKey
     * @return
     * @throws EncryptionException
     */
    public byte[] decrypt(byte[] decrypted, SecretKey secretKey)  throws EncryptionException
    { 
        try
        {  
            this.secretKey = secretKey;
            byte[] bytesToDecode = decrypted;
            byte[] emptySeed = new byte[ivSeedLength];
            System.arraycopy(bytesToDecode, 0, emptySeed, 0, ivSeedLength);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(emptySeed));
            int messageDecryptedBytesLength = bytesToDecode.length - ivSeedLength;
            byte[] messageDecryptedBytes = new byte[messageDecryptedBytesLength];
            System.arraycopy(bytesToDecode, ivSeedLength, messageDecryptedBytes, 0, messageDecryptedBytesLength);
            return cipher.doFinal(messageDecryptedBytes);            
        }
        catch(InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException ex)
        {
            throw new EncryptionException(ex.getMessage(), ex);
        }
    } 
    
    /**
     * Generates a new SecretKey object from given pass phrase.
     * @param password
     * @return
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException 
     */
    public static SecretKey generateSecretKey(String password) throws InvalidKeySpecException, NoSuchAlgorithmException
    {
        // Create Initialization vector params
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_PBKDF2_HMACSHA1);
        SecureRandom secureRandom = new SecureRandom();         
 
        // Generate key spec
        byte[] securedSeed = secureRandom.generateSeed(16);
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, HASH_ITERATIONS, KeyLength.TWO_FIFTY_SIX.getBits());        
        return new SecretKeySpec(secretKeyFactory.generateSecret(keySpec).getEncoded(), SECRET_KEY_SPEC_AES); 
    }    
    
    protected char [] toCharArray(String password)
    {
        return password.toCharArray();
    }  
    
    /**
     * Defines the type of secret key bit length.
     */
    protected enum KeyLength
    { 
        ONE_TWENTY_EIGHT(128),  ONE_NINETY_TWO(192), TWO_FIFTY_SIX(256);         
        private final int bits; 
        KeyLength(int bits) {
            this.bits = bits;
        }
 
        public int getBits() {
            return bits;
        }
    }    
    
    /**
     * Writes input stream to given output stream.
     * @param is
     * @param os
     * @throws IOException 
     */
    private static void write(InputStream is, OutputStream os) throws IOException
    {
        // Use 8K buffer
        byte[] buffer = new byte[8192];
        int bytesRead;
        while ((bytesRead = is.read(buffer)) != -1)
        {
            os.write(buffer, 0, bytesRead);
        }
        os.flush();
        os.close();
        is.close();
    }

    /**
     * Reads given input stream data and writes to given output stream.
     * @param is
     * @param os
     * @throws IOException 
     */
    public static void transform(InputStream is, OutputStream os) throws IOException
    {
        write(is, os);
    }
    
    /**
     * NIO byte stream copy.
     * @param is
     * @param os
     * @throws IOException 
     */
    public void pipe(InputStream is, OutputStream os) throws IOException
    {
        FileChannel source = ((FileInputStream) is).getChannel(); 
        FileChannel target =((FileOutputStream) os).getChannel();
        target.transferFrom(source, 0, source.size());
    }   
        
    public static void write(byte[] data, String fullFilePath) throws FileNotFoundException, IOException
    {
        InputStream is = new ByteArrayInputStream(data);
        IOUtils.copy(is, new FileOutputStream(fullFilePath));
    }
    
    public static void write(byte[] data, String fullFilePath, boolean isAppended) throws FileNotFoundException, IOException
    {
        InputStream is = new ByteArrayInputStream(data);   
        FileOutputStream fos = new FileOutputStream(fullFilePath, isAppended);
        IOUtils.copy(is, fos);
        // release
        fos.flush();
        is.close();
        fos.close();
    }    
    
    public static void write(byte[] data, OutputStream outputStream) throws FileNotFoundException, IOException
    {
        InputStream is = new ByteArrayInputStream(data);
        IOUtils.copy(is, outputStream);
        // release
        outputStream.flush();
        is.close();
        outputStream.close();
    }     
    
}
