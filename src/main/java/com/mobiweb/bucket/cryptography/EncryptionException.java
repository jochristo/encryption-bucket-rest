package com.mobiweb.bucket.cryptography;

/**
 * Exception thrown when encryption/decryption fails.
 * @author ic
 */
public class EncryptionException extends Exception {
 
    public EncryptionException() {
    }
 
    public EncryptionException(String message, Throwable throwable) {
        super(message, throwable);
    }
    
    public EncryptionException(String message) {
        super(message);
    }    
}
