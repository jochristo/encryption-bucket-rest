/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Serialization/deserialization methods.
 * @author ic
 */
public abstract class Tools
{

    public static <T extends Serializable> void serialize (T object, String filepath)
    {
        try
        {
            FileOutputStream fos = new FileOutputStream(filepath, false); //overwrite = false
            ObjectOutputStream outStream = new ObjectOutputStream(fos);
            outStream.writeObject(object);
            outStream.close();
            fos.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    public static void serialize (byte [] bytes, String filepath)
    {
        try
        {
            FileOutputStream fos = new FileOutputStream(filepath, false); //overwrite = false
            ObjectOutputStream outStream = new ObjectOutputStream(fos);
            outStream.write(bytes);
            outStream.close();
            fos.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }    
    
    /**
     *
     * @param <T>
     * @param filepath
     * @return 
     */
    public static <T extends Serializable> T deserialize (String filepath)  
    {
        try
        {
            FileInputStream fis = new FileInputStream(filepath);
            ObjectInputStream inStream = new ObjectInputStream(fis);
            T object = (T)inStream.readObject();
            inStream.close();
            fis.close();
            return object;
        }
        catch(IOException | ClassNotFoundException ex)
        {
            ex.printStackTrace();
        } 
        return null;
    }
    
    /**
     * Convert serializable POJO to byte stream.
     * @param <T>
     * @param object
     * @return
     * @throws IOException 
     */
    public static <T extends Object> byte[] toBytes(T object) throws IOException
    {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos))
        {
            out.writeObject(object);
            return bos.toByteArray();
        } 
    }

    /**
     * Convert byte stream to POJO.
     * @param <T>
     * @param bytes
     * @return
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static <T extends Object> T toObject(byte[] bytes) throws IOException, ClassNotFoundException 
    {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis))
        {
            return (T) in.readObject();
        } 
    }
  
    
}
