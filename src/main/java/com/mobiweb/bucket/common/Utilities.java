/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.commons.lang.RandomStringUtils;

    /**
     * Contains utility methods for strings, objects, and file IO common operations.
     * @author ic
     */
    public class Utilities
    {
        
    /**    
    *This method returns true if the collection is null or is empty.
    * @param collection
    * @return true | false
    */
    public static boolean isEmpty( Collection<?> collection )
    {
        return collection == null || collection.isEmpty();
    }    

    /**
    * This method returns true of the map is null or is empty.
    * @param map
    * @return true | false
    */
    public static boolean isEmpty( Map<?, ?> map )
    {
        return map == null || map.isEmpty();
    }

    /**
    * This method returns true if the object is null.
    * @param object
    * @return true | false
    */
    public static boolean isEmpty( Object object )
    {
        return (object == null);
    }

    /**
    *This method returns true if the input array is null or its length is zero.
    * @param array
    * @return true | false
    */
    public static boolean isEmpty( Object[] array )
    {
        return array == null || array.length == 0;
    }

    /**
    * This method returns true if the input string is null or its length is zero.
    * @param string
    * @return true | false
    */
    public static boolean isEmpty( String string )
    {        
        return Strings.isNullOrEmpty(string);
    }    
    
    /**
     * De-serialized JSON input stream into given type.
     * @param <T> The desired custom type
     * @param is Input stream from the request.
     * @param type The custom type.
     * @return T
     */
    public static <T extends Object> T JsonDeserialized(InputStream is, Class<T> type) 
    {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder stringBuider = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuider.append(line);
            }
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(stringBuider.toString(),type);
        } catch (IOException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }        
    
    /**
     * Returns the given input stream as  string.
     * @param is The input stream.
     * @return String The string representation of given input stream.
     */
    public static String getInputStreamAsString(InputStream is) 
    {        
        try
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder stringBuider = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null)
            {
                stringBuider.append(line);
            }            
            return stringBuider.toString();
        } catch (IOException ex)
        {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }     
    
    /**
     * Decodes given Base64-encoded input stream (image) into a buffered image.
     * @param imageString
     * @return 
     */
    public static BufferedImage decodeToImage(String imageString)
    {
        try
        {            
            BufferedImage image = null;
            byte[] imageByte;
            imageByte = Base64.getDecoder().decode(imageString);
            try (ByteArrayInputStream bis = new ByteArrayInputStream(imageByte))
            {                
                image = ImageIO.read(bis);
                return image;
            }
        }
        catch (IOException e)
        {
            return null;
        }
    }  
    
    /**
     * 
     * @param filepath
     * @param bytes 
     */
    public static void toOutputStream(String filepath, byte[] bytes)
    {         
        String filePath = "localhost:8080/complete-it/images/" + filepath;
        
        try
        {
            FileOutputStream fos = new FileOutputStream(filepath);
            BufferedOutputStream outputStream = new BufferedOutputStream(fos);
            outputStream.write(bytes);
            outputStream.close();

            System.out.println("Received file: " + filePath);

        } catch (IOException ex)
        {
            System.err.println(ex);
        }
    }     

    /**
     * Downloads file from given file path and  writes to file using BufferedInputStream.
     * It returns the written bytes.
     * @param filePath
     * @return byte
     */
    public static byte[] toByteArray(String filePath)
    {         
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream inputStream = new BufferedInputStream(fis);
            byte[] bytes = new byte[(int) file.length()];
            inputStream.read(bytes);
            inputStream.close();             
            return bytes;
        } catch (IOException ex) {
            System.err.println(ex);            
        }
        return null;
    }
    
    public static byte[] download(InputStream is)
    {
        try {
            File file = new File("images/profile_pictures/damn_128.png");            
            BufferedInputStream inputStream = new BufferedInputStream(is);
            byte[] fileBytes = new byte[(int) file.length()];
            inputStream.read(fileBytes);
            inputStream.close();             
            return fileBytes;
        } catch (IOException ex) {
            System.err.println(ex);            
        }
        return null;
    }      
       
    /**
     * Decodes given base64 string image into file and saves file in given file path with given filename.
     * @param imageString
     * @param filePath
     * @param filename
     * @return File
     * @throws IOException
     * @throws URISyntaxException 
     */
    public static File decodeToFile (String imageString, String filePath, String filename) throws IOException, URISyntaxException
    {
        File file = null;
        FileOutputStream outputStream = null;
        byte[] bytes;
        bytes = Base64.getDecoder().decode(imageString);                     
        file = new File(filePath);
        boolean mkdir = file.mkdir();
        if(mkdir)
        {
            file = new File(filePath,filename);
        }
        try
        {   
            outputStream = new FileOutputStream(file);
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();
            return file;
        }
        catch (FileNotFoundException ex)
        {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * Writes given buffered image to file in given file path.
     * @param image
     * @param filePath
     * @param filename
     * @throws IOException 
     */
    public static void writeImageToFile (BufferedImage image, String filePath, String filename) throws IOException
    {
        File file = null;           
        file = new File(filePath); 
        
        boolean mkdir = (file.exists()) ? false : file.mkdirs();
        //if(mkdir){
            file = new File(filePath,filename);
            try{
                if (!ImageIO.write(image, "PNG", file)){
                    throw new RuntimeException("Unexpected error writing image");
                }              
            }
            catch (FileNotFoundException ex){
                Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
            }                     
        //}                     
        //}
    }  
    
    /**
     * Writes given input steam to the given file location in the server.
     * @param inputStream
     * @param serverLocation 
     */
    public static void writeToFile(InputStream inputStream, String serverLocation)
    {    
        try
        {
            OutputStream outpuStream = new FileOutputStream(new File(serverLocation));
            int read = 0;
            byte[] bytes = new byte[1024];
            outpuStream = new FileOutputStream(new File(serverLocation));
            while ((read = inputStream.read(bytes)) != -1) {
                outpuStream.write(bytes, 0, read);
            }
            outpuStream.flush();
            outpuStream.close();
            } catch (IOException e) {
            }
    }    
    
    /**
     * Deletes a file if it exists in given file path.
     * @param filePath
     * @return
     * @throws IOException 
     */
    public static boolean deleteFile (String filePath) throws IOException
    {        
        File file = new File(filePath);
        boolean exists = (file.exists());
        if(exists){
            file = new File(filePath);
            return java.nio.file.Files.deleteIfExists(file.toPath());    
        }
        return false;
    }     
    
    public static boolean deletePath(String dirPath) throws IOException
    {
        // Use NIO to remove all files and parent directory denoted by given directory path
        Path path = Paths.get(dirPath);
        Files.walkFileTree(path, new FileVisitor<Path>()
        {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
            {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
            {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException
            {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException
            {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });

        return true;
    }

    /**
    * Returns the filename attribute contained in the content-disposition header.
    * @param header The request headers
    * @return String
    **/    
    public static String getFileName(MultivaluedMap<String, String> header)
    {
        String[] contentDisposition = header.getFirst("Content-Disposition").split(";");		
        for (String filename : contentDisposition)
        {
            if ((filename.trim().startsWith("filename"))) {
		String[] name = filename.split("=");
        	String finalFileName = name[1].trim().replaceAll("\"", "");
		return finalFileName;
            }
        }
        return "nofilename";
    }
    
    /**
     * Extracts first three characters from given nickname path and returns a string 
     * as a directory tree path , i.e. a\b\c
     * @param nicknamePath
     * @return String
     */
    public static String getPath(String nicknamePath)
    {
        if(!nicknamePath.isEmpty() && nicknamePath.length() >= 3)
        {
            String path = "";
            for(int i = 0; i < 3; i++){     
                char c = nicknamePath.charAt(i); // check for . character and replace with _
                char replacement = '_';
                if(c == '.'){
                    c = replacement;
                }
                path += c;
                if(path.length() < 5){
                    path += "\\";
                }
            }
            
            return path;
        }
        return null;
    }
    
    /**
     * Extracts and returns the first three characters from given string.
     * @param value
     * @return String
     */    
    public static String firstThreeChars(String value)
    {
        if(!value.isEmpty() && value.length() >= 3)
        {
            String extracted = "";
            for(int i = 0; i < 3; i++){         
                char item = value.charAt(i);
                if(value.charAt(i) == '.'){
                    item = '_';
                }
                extracted +=  item;
            }
            
            return extracted;
        }
        return null;
    }    
    
    /**
     * Extracts and returns the first three characters from given string.
     * @param value
     * @return String
     */    
    public static String createPathFromName(String value)
    {
        if(!value.isEmpty() && value.length() >= 3)
        {
            String extracted = File.separator;
            for(int i = 0; i < 3; i++){         
                char item = value.charAt(i);
                if(value.charAt(i) == '.'){
                    item = '_';
                }
                extracted +=  item + File.separator;
            }
            
            return extracted;
        }
        return null;
    }     
     
    public static String dummyImageString()
    {
        return "iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAABXoUlEQVR42uydBXwU5\\/r9Z2Z34wlJCNJCoVCkpaVCb\\/1WseJSikOCu7u7u0Nxd08guENwlwBBQkLcPZvk\\/M87M0sWCjTkliu\\/P+nndDa6u3O+z3med2Z2kQBIb\\/T\\/r97shDcAvNkJbwB4ozcAvNEbAN7oDQBv9AaAN3oDwBu9AeCN3gDwRm8AeKP\\/jwHIyoVydqeyLuvb8jNfl565\\/SdlZWVJn9fpKhnfry45fVL3L+X8WX3JtmAZSXrve8ko8cNolPK8\\/6lkJ7lI6sc75SRlfJSkNF0hOZYqLzl9+luO\\/u5\\/u\\/5bAJBfIOUFehkYT\\/Tlb93fAPA\\/AIBsZaq1yYaXAPCXEIRHxUpFf\\/KSbD+s9aInL1PKEwA+\\/V2yLfD+XwPQZLnkWOLHZwEwvgHg1QF4XqUbrLZ\\/BcCzIDz196\\/efiDZl60tOZSt8+yTLkMNpPyofzz5+ucNJKNbYUkq8cOLARgXIcmtt0r237SWnD+qYf03u1J7KS+q4P8fAORUzwfA2njDc\\/Qis1\\/0c39KgtsPgtUnqAPgRjWlfKkUCrq6i0p2\\/rC6ZFeljyS\\/96UkF\\/v2xQCMDZfk9rskmwYLJedPmAAfP4HL1+pvhlOLqQqU4f8sAJmZmX+tjEwpKzPrWQD+yvyXGWv5HaOuF\\/28dPLCTcn+o1qlCMAYPtGHVgZZa6nTR7Ukx5LfS3ZVu0hyia8l+d1vJIMw3KA8A8BnGgDtvCVTkxWSXf0\\/JKeybC8f1XB2+qTO7Rf8\\/bNUR8r9\\/xwA9wLv\\/aUCHgZI4RHhUlJKspRuztDMz8p6UUXnpNdbfsdkJWsQ1MfmfehMmZIVWi2y+7BWguPHdWBTpiaMH9SAU9naqc6f1D3hXLbWUKcy1f\\/p9Fk9d6ODh2RycpXsavaUlKKfSXLRr2i6JOU1GSTXYqV4W+Z\\/\\/HjrQw2Atjskm9\\/nSzYdDkomr22Sw9etFaeyNYs6f1i1lvNH1WY7fVzr5nNAeED115Po\\/wYAt+\\/f\\/kv537slhYaFSnFJSVJaulnOzMpUMjOzlCwBQVaWQVXO+rzyjPm2VrLRvyYFPApxrtNx1GipRJVUqXhlCNH4hFIV2xz4oUH3nm5fNixlopGmH1pIhoodJaXtIlnqsLyQsc6QL2xaza0t9\\/Bua+h7aKBz+2njS7VfOdW+xdJ5xtqTZhpqjZmgtFw7XBkV1FnueqShqeXWH0xdThaXBz1yMnS\\/IBlb75Fsflso2ZcfLDl+4WlLIH4gDOO5vcB0yLQC4R7V+P8EADcCbv2lrt+9KQXcD5Di4+Pl9JQ0OZ0QmDMIAZWVmamCkAUtEbL+2niL+XaUA+Wob+3Fz5y8cKO0a7l616R834PDX9jXv\\/fcOGz6Cq+TfhffSouLlZYtWCLZdlyQR5p2pYI86dxgeX7gVmVJ1DXTGnO8zaoU2C2JhcOSBDgujKWSYLsgHg5z42A\\/Jw52M2JhOzkcNqPuwTTiPozD7sMw6E6Koe\\/N+0qfW\\/uV3v5T5Z7+9eVuN0oY2p6STI02S7a\\/TpQcvmrDOaHWp4Shr9PHtQ9xh6fqIKykHJ9vSh1NH9f6tyjXAFy77f9SXfW\\/KZ25eEEKCHwop6WmyWlpVEqqYs4QEJiZBJmyECFQ9YJloMGq59vohjtTrlQeykX\\/3LTO58hPrQZMH7x0094KN27e5fczpYysLOlAWHqx5vuDu+b\\/I2CPcX18pP3mFOTZkgyPTQl4a2McimyIRdH1MXh3bTSKropC4eWReHtxOAr8EQaPeSFwm\\/UYeaYGwWXSIziNfwjH0ffhMCIA9kPvwHagP2z73oRN72swdb8CQ5eLyUqnS2fkjpfHye0vf6e0OmUw1Vsn2f00SHL8rCFXDtWKOJetUZswDKLZBbiV+Lmu6pL4XIWAS1KHzz0lh3JCzV+rcg1Au05NX6jW7RtJPfu2l67fvCRnZmTIcUkJcnJKqgpBOmU2m+WMzAw5I4MQZKgQMA0gZ\\/0ZAIvx9rrZean8VEFd4rYH5a7\\/jDafpMFjxK30ViW8ow5IG2KTjVsTkG9XMorvSsCHu+PxmW8CvhDi7U93xqPM1lgU3xiNQqujkJ8AuC+OgMv8MDjOCYHDzMewIwC2EwNhSwBsx9yHLQGwHUYABhGA\\/gSgzzXY9rwC226XYNP5Akwdz8HQ9jSUVn6X5Zanh8pefqUN9XdKNlXnSnbf91FhEGY7lmsi2X\\/dWbL7rq9k89NoyfjLdMlQYa4queJSSa6wRJLLL5bkXxa9NuUagObfeL1Qzb70kqpWbCz5370vZ9DsuIQ4ApDCFpBO84XxGerXzemiHWSJNFAytbngeb3eWTf+bepdqgRVUt8Wp4ro33fwizKX8zqfPNV9V2KwcXsC8vsm4sMDCfj2SCLKH09CxWPJ+OlIEr4+mIiPCMC7O+KRf3McXNfFwH5VNGyWRcG0OBLGPyJgmBsGw+wQKDMeQ5kaDIUJoIx7CIUJoAwPgDLkDhQmgNLvBhQmgNLjCpSul2DodAHGDmdhbHcGptanYGhxHEqzI0lK0yMb5abHflYaHZAMdbdIxmqLJUOtjZJSe4ck19omyTU2S3LV9ZJcZY0k\\/7qSACwjAISgPCH4ZfFrU64B6Nxh9fPVaY3Uvvl8qV2P8dLDwMdyYmKknBAbL6ckpctJ6clyqjldTk4jDGmpClcGCtOAAGQoYkDM0gCwVL69HvVvUe9RH1LlqC+pr6gvqLJUsUtxmeXbXklb+fb+pKS8+5JQhib\\/fDIJdc4lo9GFZNQ7l4KKp1JQ7nAyiu1NhIcPez0BMW2Oh7KeWhsLeXUslBUxUJZGQVlELYiAQgiUWaFQphOCKYRgQiCUsQ+gjLwHZdhdKINuQ+l\\/C0qf61B6XoXCBFAIgNz+LKQ2fpBanIDkeQxSsyOQmxyE0mg\\/lAZ79ij1fSsqv++WlLo7JaXWFkmpQRCqraP5qzXzKy\\/\\/708Ax5Hnn6+x1yRD8z+kqf37y0mJCXJKLAEIT5UTkuLZCsLlRLaChMQEJTk1WUlLEzNBOlMgQ1NmloFDoUk3340qTJXRTf+ZqkLVpGpQv4SlZtUefzdt5ecnkpPfP5qMf\\/olo+6FFLS9lorON1PheTUNVc6l4h8nU1DqaAoKH0yCq28STDsTIW2lNidA2pgAeQMhWBcH5QkE0YQgEsr8cChzCMFMJsG056TAYAIwkAD0vQGZAEhdLkHqcB4KE8Clyznk634WBbr6waPDCTi3OgLbZgcEAJDq+UD+bec2pc72cv+zACh58v5JspCzq\\/TRN19KYXcD5MSIODklNUqOjA+XI2Jj5YjIeDkxPlFOTk5SUlOZAAIAJgBnAQMBMHJlwH6f5aCb\\/45e9T\\/opjel2gpxVmhzKCpjQZtrqRGVWN21aXoLmt7vTipGBKSj\\/510dL6VRqWjL2+PuW\\/G7EcZWBSUgQWBGZh0z4wu19Lx04lUeOxKgsSBUFpvBcFyArCEKcBWoMwjALMtKRBklQIEYKjWBiQmgEQAigy7gZ9n3kG1OXfwy+QbKDfiEkr3O4Mi3U7irQ5HUbDdYbzd7hDyeu2DbX1vyLW3pci1Nk8kAC7\\/cwCIgyXPylYcQKN+\\/vwzxn+AHBkTLEeHR8hBwfHyYyommsNgkjA\\/ReE8YMigMs0ZRppv4jBol5WV6UQI3NgKCtHk93k\\/31P1qI7UIGpEjDlrztoQ881e\\/mnofoum307D2PvpmPMoHbOD0jA50IyJNHkyDZ8XbMam0HQcikjF2ehUXIlJwdWYVFyKTYNftBm7wjMwm3A0OEMQRCoQAnkdAVhFLSMEnAeUBUyBuQSAw6CaAhwGlXEEYPQ9yARAGnAL74y9gwYrH6HB0geoMtsfDf\\/wR88NARjrcx+Tfe9h2BZ\\/eM2\\/hM\\/7H4FzYx8odbcjTxMfuDbxhk2dLZCrb7hGACr\\/TwFg7yw\\/V4pJkmpWz6P+THzUWTk4IEQOCU6UYxOTlJSUFCUtPY1Vny4q3pBlzjRxILTlstCRc4ArlZ8QFCYApfn7X1N1qK7UeOqPwJSso5tCzSnzH5kxP8iMRcHpWBdmxtZwM9bQ6KWhZkwJzsS4wHRsCknG1egkhCYmITY5BfGpqUhMTUNimqbYlDQEJabhTHQ61vBvDb2ZjnL7mQbr4yCJFFipzwMLmQLzRQqwDUwnAJPZBrgakEYRgOF38cPCQLTa9Bg1Ft3H6D2PcS04ERlZmdA+slRlIRNpMCMiORlH\\/MPQdfE5eDTaDKnyWjj9vhUOdTdDqb4ukwCMJQA2\\/xMASHmc\\/ixnR0lydHjq50LD48QBIFZ8mqh6xr3a7w1cBRgzzBl2lAuNz8fKL5yVmfUezS\\/LPfYdf7c21YmamJmFDQHJWfcOR2XgYKQZ+7k9EJ2BE7EZOE758vbaiAyMZNUvDE7BrdgkJND0ZBqdYs5AekYmMvhHuNJ4WpmZVAYeJ5uxigky+EYafj6cxMGQEKwiAMujslNgjp4CXBJKEx7CMPY+qq8NQf11wWi5IQj+4Sl43gfvhkBkIY2PIdlsRnKGGUlZ6TgfGIEmk49AqrgMStW1sKu9gdvVIAB7CUCR\\/34AFMOfJSuS7KA8+ZlHIZFySjrNpuEi8gmCQe35ZrP4mg3Ndub0n59LwSI0pCQ\\/\\/5j6J1WXv99dmJ+eiW3+iZmRV+IzcTsxEzcSM6hM3EnSbp+Ky8CmyEyMooF7wpMQxypLTkvnDs9QTc7ZBw0iDMuZHO0vpuKfXCbKa7gyWMk2sDRSnwUIwKzHkKcFQWYbqLI+DNVWB6Ofb2iO74fPC+m8nwQ+vrj0NMSYUzHH9zpcaiyDVGE5TDXWQPl1JeRKy+8TgO\\/+uwF4wYfjZ46a+XdD5PQMNeqNjHhKgJCpwsAte36GHSvQLSMzsxBVnCC8zx1UjirPXdVc9PukDHjT+IRbNDwsNRPhaZkIpcLSsxDMz6\\/z67tjMjH9URouMO6TGfOi4jNybPyzGGRhImeKOqeSUWYXB8OVegosym4DEhPgi1WhqLY+FD19w3J1PwIY8Tij2YbiMtKx9dw95KtLCH5eDKXKSiiVl4MAxBOAuv9zAFi+H5YZKw74GC1i5VsSQNy2ZfQ7ppvNHmZzxjv8fnHC8D71DwLwK3+\\/ZXRa1ooT0ZlJ1xM04xPMWUjMENtMxFJBBMAvPgNLH6fgWkwSktjfRczmzvrsj6SMLNQ\\/nYJf2ArcN8VCWiFWBCIFwiERgAJ\\/hKDm1ghU3\\/CY95f7exO\\/mcqUik5OZRKkYdv5e3CtvpAQLLIAAAJgJgDN\\/6cAyOfylvr9qORYJYWxb07LYApkGtLT0rnuVyEwZZjT7Vj1zkwCD64C3ub33yUQZdiP2fuzaoakZk5ZF2xOOMreHpSSiXian8KdnZohKicT0ewLd\\/j1LRz0zkfEq8OdMP\\/v+jgbZUYpcch4TwJXBFobkBeGQ+ZqoMKWKJRdGowrYan\\/8v2oEDAJIpNSEEUI5u29BsNPs9kOFlsAAAHIpFEt\\/2cAKPRlTcnv5mU56nGIkpzMdX6aWU41p8lJXPenpKRxCZhhSk\\/PsDebM51pvoe65MvMfI8p8DnD8eeQlMy+M+6lx6ziEu50bCYCaXQiTRfVlsZBLoFGhzARDkYk42BwLKf5VHUn\\/t0frc8koMjOeHhsZAosi4REAAqtiMDna8PQ7UDk33Y\\/Yi5ISjcjLDEZoakpaD1zP6RvZlgDABolIGj4XwWALBv+JE6B0iflf5Ku31khPwx4KAeFhshhMeFyREKMHMVtbGyUITEx0ZSamm6fbk7Pk5FhzkfjixCAD7krvopMzWg++lZa0Pi76dgQYsbJmAwwDZBC483cUck0P4IwXIhLw+b7kQiKT1J3XmZW1t8OwCUuD902R6Po9jjInAMkzgH\\/4Ofvsfr9o1L\\/1vsy8\\/nFMsUec8l6OTQKJTyXQ\\/p+LiEgABVUAIRS5J8Xlqekv1O5B+A5\\/wkAjG9rv3PK76588sRh+fT5M3LA4xD5bkiQEhkVYoyPibRNS01zMqenuXE1UJAzQAlkZX7CiK84\\/Hrq5Y6XUjE5IB2rH5txg\\/0\\/MUOYDxWCCFb+7eQMrL8fjZOPo7m2T1On6tfzkYUf90QiD+cAW7YB+xWR+HB9JGpsC30t9yaGQpECj7mKmbb7EgGYDqn8Ij0FFmkQ\\/LwwjPrgvwKAl33UrlJZPv7gtHz2vJ98xO+S4n\\/rkRIZH2GIj44wxcVF2aekpjqnp6TlNaelF+ZqoDSjv+yUWylba55IQrcrqRjJSVys+ePMmvkMAZqfhVvJmfAJTcbCGyF4nKBV\\/2so\\/icf4y8z\\/ldFwmFtDAqui0a+JaGYeSHmtdyXmSDHcFXwMC4B16Ni8FnnNZD+OVtLAQsAGgSXKJf\\/WgCq2fwieVfuLl8\\/e065e+2BcvFugCH8XqgxJjzMFB0XbRsZH+eQkpKah20gvzndXBTIeGfdg9Sh3+1PQD2\\/JHheSMESrseF4SIaxeAXSgIuJ5ixN9qM8dfC4X0vTD2y93cOfs\\/7OMEhU14eAru1sSi6MQbui4JxITTl9eQNQU4g0EHxiXiYmIAxO85B+pGzQIVFWht4AsFCAcEKbqW\\/Q7k\\/F2Cyf66K2rwrlX3rF2n6xkPyhbPnDPciowxBiWGmhJhIm+i4SLv45CQnVn8ergby0\\/x8V6PTy3\\/hGx\\/77YEEVDiWhJ7XUnE3KVMd+IQe0\\/wzcWZs42Q+KzAZvU8H4mZErBr\\/4uje6\\/wISUxH3lVBMKyORuEN0Xh3eTBiks2v7f4sbeBuTBwO3X+Mt5sthfTTfCgVdQCsk+CXRV7\\/0SHQ1cPjubLPm0dycXOSJjcZJEc\\/ipfjk5KU+PgoY3xilCkuJdHOnJriaE5Ny8Pod0tMz8hXdX\\/88aLb4vDpHg2AAxEZ6lpcmC+WgEdjzFgdasZUrgraX4nBiHMPEZaQqB5Ne9Xh79atW\\/Dx8YGvry8CAgL+8ufFIeQP1gdCWhkFj7VR+HxdMHLac8LCwnDlyhUkJSXl+PGJRIvgkvBOVBxusA3UnLQL0g+zYai0RIOgghUI5RdFU8Up6V9R7k8HG4zPlcFoUltBybzvSesWr5BjI4KVxNh4Q1xijDE+PskmNdVsZzabndj1pNEXE3s7s7qKb49Fce84jLmZqsa9OOgTnJKF\\/VHipE86ht1PQ+e7aah6NBhLrwUhOin5lfp\\/QkICxo4di5o1a6JSpUqqevXqhYMHDyI9Pf0luZyJLzc8gLQ8Ag4cAn\\/cGKSv3l\\/8kcwhbvbs2fjtt9+wYsUKpKWlvdIcEJWcioDoONyKjsGwnecgV56jmm+ovDQbgmwQtv\\/HAHDJ4\\/pC5XF1lfJ6FJTG9O4gR4bck2NjYpS42FhDbFKSKTUt1RaZ6cq5sNSS7isjo9xZWR4bo1H+YDxOcfAL4qD3gNoXYcaMB2noeTsVzW+mocaVZPywJwB774UgkgCIEytZOeqtWRg1ahS+\\/fZb1fiqVavi8OHDIISIj49HSkrKSwH4dv09SEvDYEcAym989FIAuKTFyJEj8fXXX6Nhw4Yv\\/9svBSAe1yOjseL8Hbg0XKzOAcZfl6oQyAICaxAqLK5HSblVrgGoWLHiS\\/XP77+XZk\\/5QwqPiJDjY2MUVqGSnJJsSE9LMWZkpEvVd8UslRZGwHV1FN7eGIWx15JxmhV\\/gWv\\/3WFmTApIRYcbKah3OQUVL6fhH6fj8LPPLZwNDkd4Ys4BuHnzJr755ht89913+PzzzzFw4MBXmMwy8dWau5A4\\/ZuWR+JntoOXAXD27Fl8+eWXKmy1atX6EwB+fn4qJDlJgCvhUdh1+xGKdeJqoMJCmKouh7HKMiiVuSpgS9BAENslt3nbiZJyo1wDULly5RerUmWpfPny0pjZk6TTV85JXPbJ7IVycnKKgqx0adPtxM+lOaHp9ksi4LIyApX2xmAJDd8enI61j9Iw4lYKPC8m4dezSfiG+uRsCoofikJV7+u4GhqFEM4AOW0B3t7eKFGiBD755BN127dv3z\\/33hfEdAZ7ctnl\\/gQgDMalkYTh4UsBWLx4sXof5cqVQ5kyZTB58uQnf3v\\/\\/v2oV68eUlNTczQDXAyNxKEHj1Fu8BZIlf6Aqdpy2FCmKmwFIgkq6SAIICov7UNJuVGuAWCcvlgVK6kpMGzuCMnn8G4pNUlcAZwuZ2SY5ZR0s\\/Tp2vAN4sSK3eIwvLsuAm1PxmMmTZ\\/hn4IBV5Px++lEfH88AR9TpY4noujJZBTYG4ZaO67iRliUulTK6RB49OhRFCxYEMWKFUPRokXxwQcf4Pjx40++v2PHDsTGxj73dyOT0lBw\\/g22gHAYl0Wi2KL7SEx9cQWLnp8\\/f36ULFlSBaFIkSJgQajzgLjvChUqvDQBLKsA\\/8hYnH8cgeOBIfjnuJ2QqiyEkQlgW2MFbKszCaoyCdgS5CdaFixXWeZOSa+qXAPwww8\\/vFjf\\/yB9zxYwaMYQyefQbikjKUMSl4KL31t1I+kzaUqQ2Tg3BC4E4Kttkeh6KgFDLiWhy7kk1KbhX3IeKH0gHkW4fetwIjyOJsPN+zFqb7uEa2GReBiboF7Rk5NlYGJiotoCbGxskC9fPri6uqJQoUKqKSKuxXzwwsPBIfEwTrsKmfEvLhm3nx6AO5Ev7utiZVG4cGG4ubmp0BUoUEC9nSdPHiiKos4HLzsxlMhUC45Pwo3waJxhqzvxKAS\\/TN4FucYiKAKAmitgX2uFCoIAQmYa0ETI4na1FX0p6VWVawAG9er6YvXsKg3s2UVav2OJdP36mexXFGdB+nBZ6GICAJu5XOcuC0Mlnyi0PR6H1kyBmofj8fmeOLy3Ow5vUR687bo\\/Hk6HEuG8IxjVN5\\/Hxcfhao8UUZmWkbOTQKI3iwoUZ6utVbt2bRWQF30svfAY0sRrUFZEwW55DG8HYPO16Jfe18KFC2EwGNS\\/bzQaVePFbRYEoqKiXjxA6kcCHxDuy0w5v6AwnGAC\\/DJlN5TaS1WTTSoAK2Ffe6UKg4EtQWYa0EjI1Vfek2usdJJrrJLkmqtzrFwD4Lkr5i\\/V3DtaauYdJTXdGamqyobwd6QJgfHKtCA4EIBSq8NQbVc0Gh6IRc39sfhqdwyK74hGge0xcN0ZA2fvWDj6xsFuXwKctj9G+bVncYo75VZkDB6zUkTFZOXwCoCgoCCMGTNG7cMtW7bEhg0b\\/vJ3mm+5BWnabRhWxMJ2WTSkyQ\\/RyTvwL39PHGcQcJUuXRpffPEFhgwZgujo6JdWv4j\\/cEJ9m\\/3\\/fEgEThKAY5wBvpvgA2Pd5ZBotkG0AALgUHeVKhveFl+j8ZBrroJca00zSpJrr8uxcn8oeMT9v9Zwi+5J0jBqxL1+4qpa45RHcJsXjE\\/XhOPXnZGoQgi+845G6a1RKLgpCq6bouG4NRr2BMHWOw4m33g47AjFF8v9sOd2IK5HRONBTLx6McXrOhkUn5KOItPPQZofBMPyWJgWE4CZISg92x+p6TlLnpweAxAXkcalpuNRXCKuM\\/5P03y\\/4DAcuBOEj0dsh6neSkjVV0Bh1RtFAtB8p9\\/XwLHeatjUWQWlFs2vvRpyvc2H5EozJMn13Rwr9weCRt97VRmVMfcvK+MfwGZKIIerYJRbE4Yft0Tg262RKLspEoU4ELpRjusjYUcQbAmBiRAYCYGddyRKLfbD8nO3cZVLpFsclEISktTj56\\/jdPCmK4z\\/UWfVK4MNy2JhXMjHMi8S0ogb2OMf\\/TeecwSS9esBxPR\\/ISRSHf4u8zluunAX7w7YDONvqyDVXKkCYGD82xAARwLg0mAtHOuvhem3NVDqroXccIdZ\\/rpHWekVPnIPwPC7r6YRAV8ro+5liatpHac8RBFW1ierQvCPdWEou5arAbYDjxVhcFoZBrvVEbBdGwnThigYN0fDsC0GJraEwkvOY4jvBfbISPW8+T2mQGRyihqffzcC5Rey+qf4w7g8DobFMTD8IQCIgjTmHuqsvP233Y+4LEys\\/UXvv8rqF73\\/6MMQPIxPwPjdF5G\\/52YodUS8CzHqa69iS1gF+3oEoOE65Gm8AQ4NN8BYfwPkBlsg11k2VP6mu5RT5R6AobdfUXfGKiPuwjgmAM6TH6DI3Ed4f0kw3l8WgmJU\\/iVcFVD2S0NhszwMNqsiYFgTCQPTwEAI5G2xKLjmFmovPQS\\/R6E4y2FQxGVgXAKiU1JzPBDm5GP\\/rTAoQ45DWcT1v6j+RQRgPmGcEwV5Gh\\/f4EvwexD3L1e+eMxi8BPRfyMiBuf4nET1ixVAKNPtt\\/n74dFtKyRhfh1NAgZDXUY\\/AXBsuB6uzTYhT\\/PNsGu8CUqDzZAb7zgntz6myG39pJwo9wAM8n8VKcrg2+cIAkyj7iLPxHsoPPsh3p33CO8sCEJ+yoWJYL8gGDYLH7PfhnLHh8GwikasiYBCCKRNMci7JRgfzdyH1ef9cS4kXB2WxEAYxB0odmTa35AE5oxMfD3zOKRJNzTzl1CsfsNctoLZhHFGBKQhd1BhwbW\\/PC\\/w4quCtWsBxWMWxzTEc7jA53KCYAsAgmn+cf9gfDBqO1zbbSEA7O91s6VQRrYAewKQhwC4t9gKp+bbYGqyFbKnb7r8y\\/Ay8qdNJfnTZn+p3APQ\\/+arqJQy4FaqYbA\\/bEbchuv4ABScdh9vzXwIDyrPrEDYzw6EzZxHjNkg7vDHMCwKYe8N5RKMELAlyOu4FNscieKLzqDVysO4GMpJmTvsggpBrLojxVCY\\/C9cFi4+Ju27CWngUcZ+OIxL49j7CcB8AjCHmkkApvGxTAiF1OsC\\/jgZ9MpVLw73iqOYIvZF5YvHLp6DeC7HGP1ivhGPv8uqoyg63AemRhsg1V0D+TeLxLC3Bobf18KGADg120wAtsOt1U7Ye+6AoeV+yBVGdZI8SnLIK\\/qXyj0Afa7nXH2vNxOvozcMvAm7Yf5wG3sX+SYFIO+Ue3CZch8OUx7AdtoDmGY8hHH2I1YbJ2+mgcI0UNgWlOUCgnBIhOCdTQ\\/x4YSdWM9h8Lw1BIxQcYBIHB+IT0tXe2tOh0PLzx3xfwyH\\/r6QZz5g9dP8xaL6OYTOpfmzmAAzIqFMYSJN5GMZeh\\/uA\\/xwMSj2qb\\/xPNMz9ReEiPMX4tq\\/UA58YhUjYt9ivuj7YgBM5c+duBnE6t+GQnwsqvn11qqmP9my+pX662Bk73douoXm70Detj5wasUlY8t9kJtuWSc5vyVJdnn+UrkHoNeVV9DVeUqfazD2vw77IbfgOvo23Mfegcu4ADgyDWwn3INp0j0uDx\\/AMP0hKy0QCtNAYRoofxCExRoE8spwOG6IQKml51Bl6k71oNApLpdEdJ57HIFrnAnEQaJgpkGkDoKYsMUxdvMzLw0TRxHF+f4U\\/Yriq0ERKDZsF6TxV1n5NH5xnGb+\\/Bit+meI6icAkwnAeAI5JgRS7+soO94PAVEJ6uv\\/RPqIaBe9XUj8bVHt8VziiXQSxgcS0ttRsbgSFoWzwYz9QK3yL9L8OD7eRP5claneKDXpIOy8GP\\/CdE768u9WovkCAAMBsG22BS4td8C93S64tNsDmzb7obQ5cFt691vb17sK6H455+px2Y8gwNj3GhwH3YDr8FvIM9IfjiNvw3bUHZjG3IGBMBgIgmHyfe5oQsA0UNgaDITAYGkJHBBlgvDO5scoOWUPOizehzs0\\/BQnZ9E7T3N4uhQaiZusLAGCiFix08VKQfTbuNQ0db0tJA4lixhOzjTj7IMQfDhiJ6ThZ2k8h70llugX5lMzo7PNn0iNZWsa\\/Rjy8EeQul7A19NO4WpYNJekaQhPSlYP6IgkEks7sVQVj+M+K14c4xc\\/J6peTPvC+OMEQHxNwCrObnVeehClJvrirQH7NPMbrNMAqK8Zr6rBelVKo40wNdsKx5Y74dbOF64d9sGu3QEonU+nGUqVL23QX639MuUegC4Xcip3pevFMKX7JZgIgdOAa8gz5AachrAdDL0J07BbMAz3hzLqNgxj7sLARDAwDQxTdBBmsSXMIQTzg1UQlEXagFh680MUH70N3Zfuxz2afZEVJWJUxKmYosXZNJEI\\/qy2u\\/y+MEBccKmKVRhEY8KTk7H+zC0UH8RKG3ZGfWsYLfapBbGa+bNo\\/vRoKFOjoEzSq390COSROgCDHhCCM\\/hk4lHsvBmI4MQELk\\/j1CoX932TA554HAJMkVJ+OqzisYoDPuKxiUvezUyL3iuPoORYUf1cgTTeCEkY3XC9BkEDq634Gqtf4eRvarYNDi294dp+L9w6HYB9x0NQCKXtV41r2LpKktHj5co9AB3P5lSfKp3PZxrEg+p5EU79rsBlwFXYEwTTwOucC25AGXwTCmcDhYmgjL7DCrvLHS1AIARTRRoEaiDMJQTzueP\\/CIHd8nB8uPk+io3civpTtuFMQDDuc+18jtV1jDtYRKvY2eL6AVFxlxixInZvRjEd4uJx6sFjdFt5EG69NkEafYmmR3K5J4xn7M\\/TK39WjJX5TIAJevWPIgAjgiAPDYQ8hAD0v6NCUHjYAQzefUk9XH0zUjNdwHhKPa6vRb14bOJxiZSK048U3gmOQuPZvig1fhc+nOkHk9dWzfxGFrPX6VvNeFWsfoX93+i5HfatdqnV7971MBw7H4XS\\/SJsfh3Vz0ERb3n7cuUegPZ+OVUdpcNpGDqdhV3383DuTQj6XIYtZexzBUrfq1AGXIfC1qBwPlCG6yCINBBtYSIhELPBNG02MMwOUkGQ5z2G\\/dIwlNnyCKWm7cNHfVdg+NojOHH7kbrzxaXVlzhRX9Qlbp\\/hjt9+6Q76rT2MsoPXw6bfbpp7l3Ef8yTyjXP1qhexP4PmT6MmR3LyF+aHQx7NNjSS9z9MmP8Q8qB7kAcQgL5cPXQ7C+e+B\\/H9rEMYsecidtx4wCoPxaUwzicRUbgdE4uQpCQkZZg5f2TgzuNojN92Bp+N3IwP+TsfTKf5LbdBorkyp3+58QYdAn2rSnxvE7+3GQqr3+jJ6b+1L\\/J0PIC83Y\\/CuetxGHtegu1vCxfYaO9xqv\\/\\/b54B5LYncqquSruTMBICuy5n4dTjAhx6XoBNj4sw8IEqvS5D6UMI+l0jCIRgENNgKCHgclHhfGAYSwjGE4JJhGAKIZgeCONMLhVnczic8ximP8JQZEMoyq66indHbkOZPstRe\\/xG9Fq6D6M3HcO4rScwaN1htFmwC5XGbkCJfmvg0n8nbMZzJpkbphn\\/R9zTVT9D9PwotfLlydREVv94i\\/ki+vXqHyzMvwu5vz\\/kfgSg91VCcJ5JdwwFhu5HuSl78fuSw+hLk2ccuIJlJ25i6dEbGLfzHJr+we+P247Sk\\/fikz\\/OoNDwI1CasxUx1uWmwuCNutZbwSC2mvmc9AnAdhg8fWArAOh0EB49jsO1xynY9GK79Vy\\/287ZTjLaiTftkF8DAK2O5FBHR8utj8HQ7gTsO\\/nBqetZ2FOmLudg6HpejStFBeGKBgJXCspAPQ3YFgwjCMFopsFYzgUTBAiEYAohmPYIxhlMg1mcC+aEwGlJOIqsDUGplf54b9YJFBmzG+8M3YpCgzbjrSHbkX\\/kHrhN8IPjjDs0PpStJEbt88J4tepnx2rGM\\/INIvKnaMbLE2j+OK79x3AVMkqYz+of+ojm34c8kOYPuK2aL3OVI\\/M5yARb4vOTu5yEY++jyDfsCB\\/LQRQfvw8lJuxByYl7OcAewPszj1OnUXjkMdi294HUZDMkYTzX9XKTjZpUAHTz1dubNDXZwp\\/bRmB26gDshUvnwwTgJNx7sdDYam17nzznWjy\\/LCvibRteBwAtDuZU8+WWh2FscxR2HU7AodMp2Hb0g5FSOp7hxHqOQ4sFBKZBb+7EvtdZVXoaDGEaDOeAOJIQjCYE4wjBBK7TJz9UzyoapzENZnA2EO\\/kNSsMpnlcKi6OgOvSCLizRbguDoMjU8JmPpdx4oDOPCvTLRVviXvR72m8wqpX1Kqn8ePCdfNF3xfRr5s\\/KECNfrX6xbuEicdNkOXu5yB3OwO580lIHY9D6nCU7e8w7LsfhkufI8jT7whceh+CXWeu17lul2ikxElebi60RQdgkw7ABisQNmqV30RUP3+22Q7+vDcUL1+YuPRz7kTYevohX99zcOhzCXYDrt72KFfV3mTr8dImkHsAvPblTJ771sle+2FsySfdlhC0OwZjW065bU9AbncKYj54GgSmQc8r6psvyv2YBgMIwWCmwVBCMJxpMJItYYwAgS1BvFXLRA6HkwnB1GDCwOXidPHmjuLtXMIp9u1ZNH52lCqjOJgjNFOXXvGKHvfKFN6eFKUNe6LqxwrjReyHaJUvzFf7Ps0faGW+Wv2XWf0XCACrv6sfAeDz63QMcsfDkNofgNSGy7rWeyC13E3tgtTCm6KJXtshe27TzLcA0HRTdgo00dtBE73yVfO3q+bLnrugtNjL4jrIwjoOj15nUKD\\/RTj3uwqnQTcev1+9j5vHexUl2WB8DQA0882ptirN+SAJgW2rg7BpdQiGVodJ\\/xHIrY9DbnsScvtTkAUIHBSVLoSgG9Ogx2WtLTANFJEG4v34xPvyDSMEIwjBKEIwmhCMJQTjCcEEtoOJTILJhGBKCGOcMT8tjAaHUxHqIVzDdO1gjqqp1BQt6lVN0qVGvuj3VlUver5q\\/gNt6FMBsI5+mt\\/zglb93c+o8S935nPrxOfY4RBB30+x4tvugdxmN5\\/3LshcuskqADTfUxzDtwAg2oBuvgrCJq3qLeY3pfnNdvJnd4nj\\/gSAxdXmMAE4wfg\\/hwIDLnOVdQP5R\\/lHVW3ZKb\\/TW6UlW4c8rwGAJj45lY\\/SdBeMzX1h47WXS5x9jC3uEK8D3AEChKMQM4IsEqG9nwqCbA2CngbizRjVd+Uc5K+9TevQALaGe1wxEILRhGAMVwjjCMF4QjCeEEwkBJMIwSRCMDlclaIqQlvPi2UdZenzsiXux+rmi2FvmG780Ie6+TR+kFXfF62qtzD\\/IqvfYv4p3fyjavXLHQ5oALTdS\\/N9rQCgiS22WwGw9ekEaGox3tp8\\/mxTEf0+FP8O96fS8iAT9SjsO5+Ca6\\/zyD\\/oKlzYOt8e\\/zCu5cBBBV2Y\\/k6cAV40BuQegIY7cqqdSiMOK018YGq2G8ZmpLYpd0Qz7pDmAoSDViBYEoEgsC3InbhTu4rKuqyDwDTgjlcG3KIRhGDwXYJACIbf56rhIVcNgVBGP+ISMohLNq4SxnFAHMeWMD5UO4AzQShc03hNT0wXfV5M+WrVW036Q2n8EEvVWyZ+PfaF+Zy45R7nNQBE9Hc5oZmvVj+fW3s+x\\/ai+n01AKzNtwCgSgCwWQdAm\\/KfAPCU+SL6ab4n06TFASitOV9xwLbtfJqzxUXkG3wdeTg3FZ70KGb6nBUF89o4SVwISI5C0p+VewDqb82pNisNtsHQcAeMjb1haEx6G3MnNOaTeB4ILbnz2mjzgZoGHcVEzR3cTexoscO54\\/uw8voxDViJCitSEQYJo4axUkcQgpGEYBQhGM34HkMzx9DUsaGqxIEceYyu0aHZpouKF3EvDvBYR\\/4QGj+Y\\/X4Qq54JpPX8q1rsC\\/N7CvP5GLvR\\/K583F2OaQCI6GfvVwEQ0S8AaCuqn8+\\/JY1sKQCwmL\\/9mQTYomtrtvli6GtmZT5TVOFcpXC4NrKF2nIfufS5QgBuwmXoHRSdFBSxbOPWfHncXSXxYr0XKfcA1NucU61Qft8CQ\\/2tFJcuDfhkG\\/DJNOSTaeSjgdCET6gZq8STO8yLO64lq6eV3hba6WnAIVHuoqWBeF9euZcwgjHcz1+bxkWFDtZBEJE9PFA9VCuPDNIO3Kh6rCsk+7ba4\\/UBT1S8JfItk74Y9gb6a+b3s5h\\/6RnzT2vmd+Zj7iyi\\/5AW\\/QKAdnutej+fb6udGgCi\\/1ungAUAa\\/PFsKcab2V+c\\/4tT+6rFjS\\/9VECcAKG9mdgw6R07nsNeVn9LsMC8M6kkMCVO1e7Fv3QVXKh0242z1fuAai7IaeaJtfdCKXeJmqzKvl3Prn6FhB2Pg1Ccx2EFjoIaltgGrTX06Azd3pX9tzuYtklYliYIsy5rYEwMEAb1ISBgx\\/opurGqgrKvq1GfKBmuKXPWxsvev0AYf5NdUWimv9U7PPxdNdj\\/4n5et9XAeBzaadXfxumXitvLf6fAKBD4GkBQJe6LNSrvil\\/vqm3PvRp5itMS4VDtMJ9ozApjZ3Ow4aF4cz2mHfYXTiPeIDCU8KvrfReYVu8VF7J1s4kOTjYPVe5ByDnlx4Plmuvh1J3g6bfONzUpX4jCPX4RH\\/nE61vSQQ+ySYEodme7LbQ8rCWBpa2YJkNRBqItvAsCH2FYXe0I3QWGAbd14G4nw3GU5\\/f035O9PjnGd\\/3qrbO7\\/1s7J962vxOzzFfBYDPq7W3BoBIgBZWAFiWgepScLumZuK2XvXNfDTzOUSrlS\\/Mb6Gb34bL6PZnYeh8EbbcB8583HlH3IPjyIcoPSfq5Poty6R3nG304wDPV+4BqLk2p2op11wDpfZaah0EDHLtDZDrbNT0LAiNvLUZoYk+H1jSQAyJbY5rEFiGRDUNLmltoYeI5us6CDc1EEQqCBgsQAwI0DTQortPmy5iXgyYT+L+mma8ML23iP0L2rDXg\\/fdXZh\\/XDf\\/2DPm73268q3NV\\/u\\/LjX+t1uZv8Mq9nXzm+k935MF4SnMP8zeT\\/Nb0\\/y2Z2DoeAGGrldh1\\/smh7+7yMsVkYltr+KK8E3bty6UHPM4vp7rAeQaq3Oq8nL11VBqrIZcYw0IhKZahKEWYaizQU+ELRoIamvQ24KaBnufTgPLktEyJHbSh0S1LQgQ9EQQw2Lv6zoMt9ShUZ0XhPpbZDH7pma4WNaJAdPaeLHEE2t8S9V35312O6mZLwY+dejTJ3516rdUvu+fK9\\/a\\/KcAENqhA6Cv8Zvt0pZ6zXXzReV7HbEyn0vlDjS\\/82UYe9yAHSF3G\\/4AHmIpzKG35tqIKZtGjpScldcFQLWVOVUJuerKVLnaKjxRdWsYLCAwDeqKNNiipYFlUBRpIFYLYjbwsk4D7vh2NKKDaAs0pbM4AidWCwIEYZww8KoVCM\\/TNSvpMf9kur+om35OO7qnGn8qe9K3qDOh7HhQr\\/79evX7Zg996uDnnW36kyHwGfM9dz45uicO8KjGN9+rV\\/4hzfwWJwgAzW9zhtF\\/AUqny6z+GzD1vg1HtjF3LoE9Jj6GNC4SvbwD2w9tV0+yeXI+8O9uAVVW5FR21H0KcpWVIAyangWhlqU16POBZVBsaJ0GejW04A4XRxO5BFLTQIWAbaGTOBAjEkEcjxcrBgsMl\\/WjdVe0EzaqyZbt5Wcq\\/UL2ut4y5HW1GG9V9Zae\\/8T8fVbRv1uPfuvBb6dmfivLAaAduvn83NNbl26+p268AN6Lke91VDffj9XPym9H8ztcgdLlOgw9\\/WHT\\/x5cuOrxmPAYblPCYJgUg0E+F76v2uAryWgyvKYE+HVZzlV5uY8s3v\\/2V10qDCueAcGSBhueng1UCPTZoImIRn0YasGd04pp0Jrx21akAc1pf1IHQSSCDkM3AQMN7X5BO1bf46J+5M7yuaXK9UrvcTY76lXjT2YPek\\/M53124n13tFS9MN8S+7r5KgB69VukHgCivHZmV70Xf8bzmUFPj3xZVL3XMSvzzzH6L7L6aX6nGzB084exTwDsBj+E6+hgeEwOQ56pEXCYFRszbNuZt8u895Fe6coLLwrLPQCVlr6KhlKQKy2DXFmXCoMFBNEaLGnwTEv4nS2h\\/jYNgsYWCHZrVeLFndWSJrSmGW0ECKzK9gShAw3rKI7Hi5MyNLILYeiqS5yp63ZW31pJHMYV6\\/luz1S8ZcgTFa8ab6n6\\/U9X\\/VPm++i9f6e2tcwBT8wXW28r863W93rkq+aLf3GsBSO\\/5Wkr869C6XiD1e\\/P6g+Aqf8D2I8IhuuEUOSdFgG7qVEovizh1Cyfg3JRt08kD8PbUj5jkRcq9wBUXPIqqqi9nclSTZV0WUB4kgaEoLpVSxADolg2irmgPtOg4XYdAqvp+CkIaE5bmtROHIkTZ+JoYEdxaPakenpW7nxKO1avHrLVt10tW73S1Zg\\/\\/sR0haarYsUrwviOetx32Pv8qrdM\\/c9KnPzx0qNeGO+1S4v85r7ZMHPK18zXI181n\\/2+NSO\\/7aVs87vehtIjAEo\\/TvxDguE4JpTRHwn3mdGQp8Xht73JMxbOHiI5u7hJ7nkLSnk93nqhcg+AeB\\/7nMudClff+Fi89WmFJdr721ggsLQH65bwFAQbxStfsyFotFOHQO+ZLbgDxQsiWtGcNtyBbQlCO4LQXkzn4uDMMU2djmsSp2rFCZvO1hXOqKXhhq5HYep6RJWNUJdD1EHYdBbaD1OnfTB13Atjxz0wdPCFod1uDmW7rCp\\/py6rylfP\\/OkAtODPtRAncnz15d1e9bCuGvmefNyewvyTWtW3ZL9vJcy\\/TF1n3+dqpQuXq8L8Pg9gGBQEmxGhcJoQAfcZ0XCfGwdpVjIGHAus49m3mmSwUSQHGwfK8YX6dwEg\\/pHC7SoAFlkgqKi3hqeSYFX2cCiOG9TVk+B3CwTbxGvgCIG3DoHYoXs0EFqxklpzh7YhDG3FqVju1PaHs2HowG1HizhgUQYu40xdDsOu2xE49jwKl95HkafPMbj24bb3EeTpdRguvQ7BuedBOPU4AMdu+2DfZS9sO\\/rCpsNuGNvvgrHdLhjaehOGF5jfwifb\\/Ba+6pm8J8Z7aZEvq\\/2eVd9CmH+e5jPy2wjzr7H6uVztdIet6h5XKw+hDOByb1gI7MaEs+9Hw2N2LPLMS4Dj4qToDeceFPyhfhVJdrORbPI5v1S5B+DnRa+qNk\\/e5fJPIDynHVTXIaglIFinQVCPENRnO2hAABpt1yBoqh8w8dSrquWzEIjj8Qd1iRM0h7QTNVyzK5SBE7wtq9u5+2F49DuGQkNPofjYsyg18Rw+mHyeOofSk86h5ISzKDb2NIqMPIFCQ46hwIDDcO+1H87d9sK+4y7YtPeBUQXAauBr6aMb75NtfAu9bXnp5z1aaMbLT\\/r9Gd18Rn6bK2rly+1087vd54olEHK\\/IChDQmAcGQH7iVFwnRULjwUJsFuQhM+3Ju04s+eMVKpEaUk9B2x4uf4FAF75jYkLUwkUCMPTIFhDUPkZCGrqENRZnw2BSIIGTIJGIgm2a8fLm+nLKC99R7fijm5NGFpzZ7cRp2P369r3RIZ2e2HX6QBcex1Rjf9wykV8v9QflTfeR8X19\\/HD8tv4ct5lfDbtND6eeAxlxx7CR+OP4qMJJ1Bm4mmUnnAG743x4+8eR97eB+HYZQ9sRRK0ekHVPzH\\/oLq8E71e9jquR76fHvk0vzXNb3uVxgvz\\/ZlUNL8rze9J8\\/sGQx4UAmVEOExjo+EwNRbu8+KRb3Ei5IWpGHQqymvRotGSydb4et8fQP5pYW60hcKfIHh2JmASKFXV971hCggIOBPU1iH4jRD8zpmgPiFouEWDoMl27Vy5ehRNH7DETm\\/pq6mVOA8vzsgJ7VUvzjC0FRG+Dy49DtHAk\\/h87jUa\\/xA\\/LruO94fvQf5WC+FUeywcqgyBc7WhcKszCnnrjYVHw8nI33QaCnjORr7mc5Cv1WIU7LwOb\\/fZhbcHHYEH24YTZwVjq12QxLBnMV60J8uBLC9L1XMWeVL157TIb31FjXy53Q3d\\/Lvs+zS\\/B83vQ\\/MHhkAeKl6cEgXjxFgu+eLhvjAR7kuT4L46NfzAtcgCA1f1kRRX\\/QqQv3hp0L8AwB+5UU0VgBdCoK8OdACUapYUWC3e\\/4YAiFbwHAgab30Ggp3a8uoJCLs1CHQQDG1ZqR32wJU9vfTECyi\\/9h5+WnQBxbosg0OlAbAv3wv5G4zDh\\/3W4KfZR1F7\\/XU08rmHpnsC0dg3EPV33kfNDf74aeE5lBt\\/AO\\/334x32i6CR5NZyOu1GAW6+yBf3+Nw7HxINV7ST+Ko1zuIqmfcy5a1vWXQE5HfmlXfRo\\/89hz2OgWw8h+w8h\\/R\\/Mc0PxTykHDII6OgjI+BiRO\\/49wEuC9Jgu2SFNTal7owJipAatquniT+\\/Q4Tl\\/\\/iONDLlHsAfvwjN7KlbmRD8OIUUKoQAvHuV+pAKADQU+ApANgOxJsiNLJAsE2\\/Zm5H9hE2Sw8W\\/Vi8erb1bvZsXxQechxfL7yJHxdeQIlOf8Dhlx5wrzUUX47cgobbb6HTmSj0uZaI3lfi0ZW32x4NRZPdD1Fvxz1U33gbv65lq1h9Cz8tvYGv519T9flUP5TssxkFmsyAe8OZKNhtF+cEP97nEabBYauqt0z5lqq\\/TDBZ9W1vZJvfkcNeF5rfQzd\\/gG7+CHHNYgwMk+NgMzMezqL6lyfDdnlaxpa7ad+dP3VQ+rlKBSmPi4NqsK3p5fp3AyDUhUmQDYAKwZ9nARUA0QbUFHjyLlhMAQEBk6Deeg2CBoSgoQ5BE8vVM\\/qZNXG0zUs\\/+NJip9qbHWh+yXFnaNgVlOixCo7le8Cl2mB8PGwH6nvfR\\/uTUWh3PAxNdz9ArQ238POiS\\/hi+ml8OO4Yig09iMID9qFgnz0o1H8\\/3ht+BKVHn0SJUSdRkvpg\\/Bl8Pusavph9FWUG70K++pPh1mgB3Huc5JB4mhDQ\\/JZ65LfSp\\/xWV3TzucRry8hvd1c3n5HfPYgTP83vT\\/MH0\\/zhkZDHRMMwKQ7GGfGwn5+IPEuT4bA8Dd\\/4pOyLehimTOo0WLazs5VM9kbJaGuQjHYv138CADcq6Ok28AwAlbIBUKrps4ClDdTWAfiNANRjEtS3hkC8RYoOgnpOXT\\/N6qnJkdP6e6M5vA0\\/iDw1h8O5Um+U6r0ev66+iQY+gfh15VV8MuEoSg7diyJ9fVCg61a4t9+IPK3XwbnlGjh6rYaD5yrYe\\/J2i\\/VwbrMZrh13wqP7HhTsvR+F+x9CUc4B7w0\\/hbJTruDzGVdRrOMazg+T4dhGXL17Xov8liLyWfWt9Mhvy6pvy6pvx8jvwH7fWZjPft+b\\/b5\\/GIe+CMjDoiCPjoGims+Jn+a7LEmGx8pU2KxIx9xraTUTfG9INYtVlP8tbxL1LwBALez10gQQAKhzgHUKrNJSoDYhEO+I9ZueAuqbIxGChpt0AKyvotVBaLYVdlyeFRtxCoU707iKvVGszSz8uOAcam66i4rLLuOj0QdQoAvN9loG56YcABvNh2ODOXBqMBtO9WfCkbJsHevPUr\\/n0GAeHBovgkPT5XDyXI08rTYgb\\/ttBGcXCvXah+KDT+DDCddQot8+eNSbAVvP\\/Yx7VrtqPretGflthPmc8tux6js+0MzvRvN70fx+NH+gMJ+VPyoaykSaPz0B9nOT4LIoGflovsuqdHzhk340ODrNtGO0j\\/K2wfXfA4Dyy1JNPy+RFE74rygX6t7TACx5CgCxHFS4HFS4HFTUo4M6AE9mgXXPgSD7hZPa1bRb1CtsDZ7bkL\\/PQeT3Woj8NQfi67HeqLL6Boe4CygzYi8KdloPl+aLaPpcODeYCef60+FSfxo1FS6\\/T+Pn1pqua4YqAYgjQXBsMJ+\\/vwguzVbAtcVauLfaiLxttyIfU6dw3xMo2vco3BosgqnZIdi1F8s8YT6rvu1dzfwOwvxHWuUL8\\/uGquYrQznwiaFvQixMNN+B5rstTkbBVSl4a10aHNaas1bcif\\/1TuRd6bfmzRXpFT9yD8C3kzV9N01SKqySlIprslWBKr+cRi96mZoq4l\\/JtADwBIJsAOQnADyzGni2DagDod4KrAFQX0O3BU4dd8O98WwU85qEH2Yfx\\/fzz+NDYXzHtWq1OzacSwNn66ZOhfPvwnihKerWYrgT4XBqMIuaoyaAYwPxewvg0FD8jT8oQtR4KbUCTk1W8G+vhHOTlXBpuhqurbYTMoJWazxc2uyGQUR\\/G0Z+O0Z+x4c0Pyi78mm+QvMNNN\\/EyjfRfNtp8XCi+XlpfmGa\\/+7GNORZZ0al\\/ambYmPD5JGblys2hdxk9RpwB0OOlft3CjXYajI5SvI7FSW5WG1JfrempmK1JOUfQyWl0joCsZpJwZT4edGzkqlDGgSLwZ\\/JBqDiMvEPKGtnC6uuUhNAqW5ZDbwIgE3PzAKabDn4uTdZgDLdFuHbWezNjPpCXTcgT7OFuoFznkhEfHaFa1JjX3y\\/4TzVaPtGi6kl1FJdy1Q5NFxCLaWWw7ERh8vGnBcar4V9k\\/Wq7Bpz23gNgZrO7Ua2AjHts+o7BOrmc9jrGQKlTygMNN+G5ttxrW8v1vqsfBean4\\/mF1mdgpIbU1F0UzrybjLH77sX++H1ixelot\\/+pFW\\/QX4lvZZ\\/Nk57ILaSXKSKJJdpJyk\\/LmAiMCV+Wa63jCcpUJbmJyusfoXmKzRfqUDzKy7XTxCJ08UCgNUEYA2UmlaHhi1HBsWJIisAFLYBY+MN3G6ADavftdlSlOGg98WUEyjeh5HcehnyNJn\\/TBULc+erevrzBbAXptNcu0bLYUtTbZuspdZR66kNquwar6Opq7ml4bxvO6aPbdMdsBWvhmp+EEYu\\/4wtuBz08oON52EYW57XK1+YH6ybz8jvHQbjgAjYcuBz5LTvMikWrpz283Lge4tr\\/eJrUvDB5lS8vzUNzusz0OtMUv+4kGCpZcsJyktfAvw6WsArfdgXIAyV2S6majNAxbVsE6v1GWJxf1H9SnnNfIXmK5UY\\/ZUZ++KfUq8irhWg+TXWEgBKvXLIci3hRv108Sb1oJB4o0RTo41wacXY91zPKlyK4t034oOhe\\/BW+1Vwa7oAzox7J4vJqoTJf1CLqMW6hOFLNdMbraTxa2BDw22abKQ2w4azhQ0HTBsuOW2abqfZQtv07U6avhum5gdgUo0\\/o038ouJb32Hvv6tVfsdHuvkhmvl9wmEQ5g+PgtPYGOSdEosCs+Pw9oIEFF2WhNJrU\\/DxllR8vD0Nb28246s95mOBYaH2o6cwSW3y5cr8fx8AT1LBRpIcC0ly0WqSUq4\\/YVgg2oRBqbhy358BYOX\\/ysqvyqqvRuNrrCMA67Urhmpv1C8ktVxevlk\\/P7AFJsZ+3rbb4NZsCaN\\/Ht7qKOJ+MSOcldxgsSq7hprsGyx6Yrwd49uu4TKavoKmr9JMZ2XbsMJtmmxS34TRRJNNNNmG1W0jjG7qzZTxoXZRuylR8ftpPquca37V\\/JaXCYAY+Gh8W1Z9e1Z9xyAr8znp9xbmR6qVL8zPNzUO78yLR\\/HFCSi9IhEfrUtGOZr\\/+Y40lNqWjoJbMmL23I\\/7aPGy+ZKzxzu5Nv\\/fD8CzH45vS\\/IHLUUyFFUqrQkmCE+qXzW\\/Ciu\\/Ks2vRvNrrCcA7J21aXodml13S\\/aVxOr1g1vVE0SGRlvh0Gyz2ovFICbi2dRQaD3h2KC2BxOj2kaYTLPtGtLwhqzyRqtpOL\\/O79uIN19ipaumN9lO7aDx3pR4feMu9TWOpmZ7aLjQPmq\\/VvHND8Hkyar3OgVDi7PUJdV8pbVY5ol+\\/0gzv4tufo9wms9Jv38kTEOj4TQmFh5T4lB0XgLeX5qIj1cnodzGFHy1LQ1fe6fhU+90uG3KwLRbGZ4nD\\/lIbvld\\/mUL\\/rMAWCWDXLRqJaXKJrNSicZX1s2vIsyn8dXZ02vQ\\/Jrs8bW3QKlD\\/bYNSj2h7QRgh\\/aaAnE5eYMd7P\\/itYgCBn6\\/ET9vLLSd4tcbbyUEIiloMocxTZthpNlGmm1sshNGmm0Ur2huupvypfbASKONNNooejqNNooqb36EptNwz+M0\\/aTa4w1ewviLrPyrNP8WzReVr5vficZ3Yb\\/vxsjvSfP7cH3PyjcOiYLDaMb+5FiaH4+PWPVfsOq\\/2ZyM77an4vtd6fjSJx1vbc1AyxMp0y9c9pO+KfvB37Lr\\/9MAWF+jLMkfte+kVKdRv7Liq9D4arrxNWh8Tfb4WsL8rVDq0tjfaGo96vedUOp7Uz6cAaiGuyhuGwlpL0JVmlBi29hHfZWyoYm3ZrS6FZ\\/vgoFGG5rupcl7YaDZhmYHqIPUIRhotKH5URhotKH5SW5P0fTTNPsMzT4HpYU4wndRi\\/sWV\\/Wq5xq\\/Ddf37bjE68Cq70Tju7Dqu4U9Zb5haCSn\\/Wi40fwi8+JofgK+3piEH7am4IcdlE8avt2djqLbM1F5f7r3kYsX7H6oUuFP++5\\/DYDnXaQu3sxGkj\\/rOVapRbOq0fTqNL0mDa\\/Fyq3D6qy7AwbVeJr+Ow2uT7MbCPHnG\\/pqarSHRlu0l+YL7ctWU5orJExuup86qEkY3eywZnSzY9wKs09QjHNPUdmnter2Oq9VuIj3lle0Sm95nbqhV7zF+AfZ\\/V41n1Xfneb3iuA6X5gfBYWxbzMqBq5TtJ4vKv8bRv4v21NQ3icVP1Hf0fySOzPxz73pZ8\\/eeZCv3aTu4iLf5++\\/\\/3IA5JdI0SWrxxV+mDxfqbefxtPs2jthS6PtG+6GHY02UYb6NLwBDW5INaLBjWhsY2Ewf6fJAZps0UHqULZosKYj1FHqGHUcCo1W5Sm2p6jTujjEeeoVrhp+WTVcPYzb6jp1k\\/KnaLqI+tb3tLhvF5gd+Z1Z9V1pfPdwzfx+UZAHRkMeGqOa7zI5HoXY88uuTMQ\\/NyWjAqu+Eo3\\/eXcq\\/rkrDe\\/T\\/C\\/3ZFzf5XejRJ+xAyTpHXvlqf31Yv3XAJAT460vXJckW1dZ+nHqAqneQTixWt28DsDF8wCc2H\\/taKyJRhuE0U10g5tZmdv8iGYwK1lhf1blKbY02vO4ZrLnSeqULj\\/qtCZGuuJFw1nlitcFTS0uWhkvTOdEz0rXjOdw1zqAFc\\/pvg2Nb0vj2wdlV33nUM38ntqwp5o\\/SDPfNDoWLlPiUZjmf0Lzf2S\\/r+Kdgqo0vTzN\\/3ZXKkp7Z+LbPenXd\\/hd\\/mj2iOGSo6uHyWo\\/5QQC+T8JQE6Mf\\/YaFaP+vgWyvZOr\\/FW3LfPc2p+FR7sTyNv+BFy5dW59AvYtj8HU4qj2oglxSZVF6qXUJ7Olnnr1y5a42FIszdTl2RntzNyTHi6q\\/IJVL7+cHfHC+NY3deMt1W4x\\/qFW8e0fZfd6YXwXGt+NxveM0Pp9f5o\\/mOYP08x3nRqPolzjl1udjPLs99V8UlB9dxoq0PxvCEEJH+AH3+QLGw8cLzNt7FBDkQKFba3e49PwDAi5huB1APCqxlu\\/eakw38ZgMNh26djGZce2ze51x+0f79btCvJ3v0RdgHvXC3DpeBZ2Hc7A1I4mtjmtvVZOvFiyjbXOPSNxrd157Rp7VRc1iStxWl1WL8rQttdUw0VfV6tdGC8O4ogTN2I5J47fi\\/W8GvUPs43vGJzd60XV60s8rd\\/T+MExUIbHwnZsLPJOi0fJRYn4ej2rfkcqavumooZvOis\\/jdN+GorvBvt\\/9IGl6zaWGty7l907xYo6iFy0emOP54FgyE1b+HcC8CLzLU\\/KRpdtiffecxo4sF++iRPGF5g\\/b6a75\\/itXd8acCs+X\\/8AFOh\\/C+59bsC55zXYdb8KY5fL6itklc7i5VJXsrcdrdRBl3hJlXhxhWXb7pp2ybUqmt6Gg5y4MEOcom3jr0mcqhVH71Tpw51qfOAzxoteH5rd60XV9xX9PgbykFjII+JgPy4O+Wdy2OMa\\/+fNKajFmK+3Nx01qJ99zfhsVybepflVNvovmThleuFWDRs558\\/\\/lrM4lkrZ6RDYPAOB8RXbwmsFIDdxb6M\\/MVv9iTp4FHjH+btvf3Jv+HvjAr279Sw8anDffG2HzalZcuTVW3lHh6DA8IdwH3IfLoPuw65fAIy970DpJXQbSk8hfyg9hDiVd+e2m66u\\/LwL1VlXJ36tI7cdxZYV3kGIhncQh2sDdNH09ve107XtH2YfzFEHPCvj1eWdlfH9RdXT+KGxMIyKg+OkBLwzNwlfMvJrcNBruC8NDQ6YUWNfBr7fnYEyvkCx3VkJdebu7tepQ3uPH37+Oa+zm7sb94ez\\/n5ODn8BgiE3IPxdAOQm8q3Nt9OfnHiiTvxVF0d7R7fSpd7L\\/+NPPxaqW+e3Yh08G7\\/TrmuvL\\/8xcv+m\\/FPDkX9SKDzGhcCFQNgND4FxyGMog2jMwEBVyoCHUPo\\/UF8+pfR9oL6SRul9n5DcIxjiKlsa253brvc0ddHVWZyXp9GdHminaTsJBWrn6sVZO4vplqgXFd9Tr3qL8WLKH8LKHxEL09h4uE9LxPuLOeFvTkVTVnubw2Y0PZSBqjT\\/yz3Ae\\/uB0uuCL9XsNaZW7UoV8pUo9t7bJifH\\/NwX7pQr5aLtl6dAsH0OCK8MwesEIKfm2+tPylGn3UV\\/0nmNJjm\\/o7vrWyXff7\\/ot998XbJ2jSqlPZs1+qBy75l9is24G5iPQ1S+WZEcqCLgMCkKNuOjYBgjrp6hESNpyHAaMyxcvYxaHhyqXk8vDxDX2FH9grVr7HuLc\\/A0tifVI0i7Dk+oG9U1OFvi0G03fS0v+nsP3fTeuvH9aPqAWK3qh7HqR8fDcWICCs\\/l2n5tChpxPd\\/1mBk9Tmai+dFMVNqfhY8PAO\\/6ZqV8NHrjnO\\/LV\\/7k41LvFXZxyVOUu68Qn38BSrzPa14rCJx1EBz0orH7F5PgbwHgZdX\\/ipX\\/xHx3\\/ckXkGTlbRt7x6L5CxYq8UGZMmW+\\/f77z2pU\\/qVsjUYtqpYbsW1DoWVRaflXJMFjIdfU82JgOysWxmlxUCYnQGLsShOocfGQxsRBGi2qkkYNF0sxgjIkQrvkaiDN7B+uXYIlrsTpI07QhGrqFaZLH+pExFtivp+l4oXx\\/PtD4vn34\\/H\\/qrv2oKiuO7z33t1lH8Dy2mXZBy7LsruwgDzkrUgwRCUianwrPqLEhoRaI7WSVDGKKPUBosSGRDPkMSY+atI4Y8c0wWhsY1utcZq0tdN2Mum0f2RsakQeC8uvvyvnzpzc2ReCSpj5ZhGWvfec7\\/t93+\\/ce3YN2YXCbOuBzE7M+DP9sOHCADT8bhB+fNkDVRcApnQB2JD8+MNXLtnKlyy1xuls0ZoIJ8dJk3DMFoQJYSAi0IqcwFskBBLBfREAE0TH763TF5o9Mfn8wPjPNI0kquctMI5MhoVhOJtCqU6O1ZsmOpwpeblZGQVlUwqLSlfWPpvWev4T46nuIe0pN2jevA3KI9+CvKMb2JfvgKS9ByQHe0FyoA8kLYi9+LOfIZAkphGB+cxbNb88Y7YS6+bX6s\\/fHEb9zeEl3Gb8ef03w00dT\\/gLt+6SzmxF0hu6QfJiN7pPN7pRDzZ5vTD3l26ou+iG3VcHoQmxAcmv\\/AQgA8m3vHbjLxOWPteg1+vyVVJJukwWkolj5N\\/M70DYKBHEkXmI8RMHdE8w4sZwtA4wUgGIq1\\/lo\\/q1RP18FcQjEhB8dSRznDxdHRo2KSY6qjDebC7NdCWXlU4uqixa9aNtzoNdl\\/Vnbrs12EmrftEP8mN3gHujB9jOXmBeRXT0AXO4HySHEG2I\\/Yg9KI7mHmB2I5ruoCi6MT5u361kpuHWMLYSogU0oKts40lHIW3H12\\/qgdD9fbi064dZpwdgE1p96zUPtF33wJarAFW\\/B5iMle84+ucblqWbW2ONxgo1JyliGMkUHFM+IguRjkgh47SScfNRoCfzES0SQKgXAcj8CIAdawGMZL0fSADfyX5R9U8gE2JHuBAZiGyO4wpVCkVJdGT0DGuidR4KYUFxXs6K3IWrG5OajnVpT\\/3rv2qsNsUHAPL3BkB6vA\\/YY30oiH7gOt3AHUV0uIF9yQ3MITdIWlEM+3iHQJE042MTCmMnEtzIi6JnGDvw+50opMZeJL0PZPg8TUs\\/OI64YTYeo\\/6SB9o\\/Q3w+BFs+A1iOxE8519+f0tL1R\\/PsNa06XewyZK2cZSTlOIYyxFREIWISGZfgAomiKNCReYkkLhnmQwDy8eIA\\/tb8\\/hxAsH8tEYCRVIKVTAwvgIm8ABAFjIQp4VhpmUqtrojVxy5MstlWZ6WlrivMTK\\/NLi59MXHNpuPaA2c\\/17z\\/9Z3wjwE05wFCsetWvzcIqpMDoD7mBvWbg6DsHICQVwdA\\/jLipQGQtaFgWt0gw8iQI8GKFjeo8PswjJHIg\\/2gbx8A+5FBmPrOIKw554HmK0Nw9AZAx98BGv4EsLhrYDC349pXiet2ndVnFO7CJU01DnwZnvNixDzELMRjiBJEESKHEoCTigEzJQAhBjSiPiBYB3ioESAWgDwIAcQSAZgpB0ghVplNrLMYMQ2PVi6XyeZpNBHLTCbzOleqa2Nmalp9tiNpS5o9cactt+iVpCU153J2v\\/HX\\/BPXv8n46NtB+0UAK4rCgi6RgKJIeB+XY6exOTuBj+8MQeIxDyS95YEUfJz49hDkn\\/BA2bsAi\\/C5Nfi3jVjhHX8D6PwnwMEvAOo+\\/E\\/PnPYPv8pc23DJmF3yerhG04iDrMNJ+CGe49OIJxFLAwjARcaZSIQv9AFaLw5AC0Dupwfg7qcAxmIVQMeARiQCPRUDiSIXyCGTV0Ims4JjuQVqlWqFVqd7OtFqq0txpmxLS0ttTnU6WpMt8e3OCcYjqSnO49mPlP06r6rmav7m1n8UHjjzdfHrV26Xnv6yf\\/qvbnpmf9QDT3w8AIsvDMFKbNrWXgKo\\/c0QbP7UAzsu98KeT\\/\\/n2Xv+y74d7\\/7h1vpDJ\\/89d\\/32L3JmzL9otjlOYaW\\/goPbj+eyE9GA+IlkWABPIVYgFiIqETMQjyAmI\\/IQmX6qX1gOCuSHU\\/av9CGAoC8P328B+BKBeCUQSARCFCT6cIIpd50AJ5ZhmEq5PGRxVGTkKoPeUGOxWDbanfafOh2ORqfTuddht7c5bdafu2zWo+l261tZLufJvJysM5OLiz8oLa+4MHPR8ssVK6qvLKiuvba4ZuP1Zc\\/UXa965rlrVU\\/VXp2\\/dNVvp5dXnM\\/LzztrTYg\\/ERmu7pQzksN43BbEbsR2xAuIjYhnEdWE+EWIuYjHRdlPV754BeDN+ukVgHgJONKLQZIHdSHInxOE+BCBtzgwkKqweHEDXgi5xBH4yX1UynHlCoViTnh4xKJorW5lnN6w1mQy1hgNpvX4WGc2m+qNZtMWg9G0zWQ0NpqMhl0Gva7ZGKvba9DF7DXGxuzTa6P266Ij98VEReyJCAttVisVTTKpdDt273x1P4\\/YhNiAqEX8ALGGEM5b\\/QLEHEL6Y6Tiha4\\/m5y3i6r6BB+dv7fGT+lj+TeSm0MP5FLwKC4H+3QDgygWeEdIJhb6HTHgwaeyLDtNLpVNDwmRPa5UKCtD1aFPKJXqRUqlYkmIQrFcJpevkEplq6Qy6ZMcy65lGLYaz7qa2PY68sj\\/ey3J8dWIlYjlhGi+uueTCq9AlBPCpxExFhHShU4\\/jZyvner2zRTx\\/tb9Si9dv8wP+WygG0IP4mYQ4+fTCsWNYbBCiKWWifGUK9hJRblIRPDZOokZztkCPnMxIopJNU4jdjwdMZMQN4uAJ3I2hQqCWaSiZ5Ic54l+lLxeMcn0AiLASeT46eR8BNLpahdf8YsWZX2Yj+v\\/gSw\\/YOU\\/rP0AjA8RiG8Li6MhVHStgBaDjnIGM3GGBEoQDtIzpBIyMggx2SSDc0mFFpBcLiRVK4bwuwLy\\/DyK6CyK7FTK2oUqTyDnJVR6nBfSxdf7xcTLx+oO4MPeEcT6+exSf46gErmCWAwxlDvQUSGIwkKWkzZytU1wi2QikBRCXCqFNIJUilgXeW4yRXISeV0rRTZd4QLhOuoGTyDSxev7Md0IMp72BHIBNoqIXUHsDHRMRFCXkwVR6CiXiCOEGClxxFOwBIEJ1PPN5HWMFNF6imyhwqNEhIf7aOq85Ttt9VLRfI1qP+CD3BXMSIK\\/ecT62S7mKybEDkGLQkOJI4oSB+0aWkooAmIp6LxAS\\/4+hiI5ihKhxgvZQoWrqPOmCZcHme\\/sWJD\\/sN4XwEhGvlM4WEF4E4U3YYRRMRIuEopYNDShNGhywyiSQ6n\\/lY0mWryJQx5ge9dI830cvzt4bATBBegbZL72GIriQ+FDJPcCpci+FdTxAhHtL8+DzXVmtJM\\/Pt4bGFxE+HIJfw2l1I9ryEQE3SvEr0kfy19VcyO09DF5K9h4FcBoRRGMIAKJ414R6FjseCL8+ySAsRAHPdmsD6Gw9wHMPeChfH1fBTCa+BiPkIxXAfwfL\\/xzlMo21nIAAAAASUVORK5CYII=";
    }

    public static String toString(@NotNull long value)
    {
        return String.valueOf(value);
    }
    
    public static String toString(@NotNull int value)
    {
        return String.valueOf(value);
    }    
    
    public static String toString(char value)
    {
        return String.valueOf(value);
    }      
    
    public static long toLong(@NotNull String value)
    {
        return Long.parseLong(value);
    }    
    
    public static long toInt(@NotNull String value)
    {
        return Integer.parseInt(value);
    }   
    
    public static boolean isEqual(@NotNull String a, @NotNull String b)
    {
        return (a.compareTo(b) == 0);
    }
    
    public static boolean isEqual(@NotNull String a, @NotNull long b)
    {
        return (a.compareTo(String.valueOf(b)) == 0);
    }    
    
    public static boolean isEqual(@NotNull String a, @NotNull int b)
    {
        return (a.compareTo(String.valueOf(b)) == 0);
    }    
    
    /**
     * Generates random alphanumeric filename appended to given filename.
     * @param filename
     * @return 
     */
    public static String randomFilename(String filename)
    {
        if(filename.length() >=3 ){
            char first = filename.charAt(0);
            char second = filename.charAt(1);
            char third = filename.charAt(2);
            String random = "";
            if(!isEmpty(first) && first == '.')
            {
                first = '_';
            }
            if(!isEmpty(second) && second == '.')
            {
                second = '_';
            }
            if(!isEmpty(third) && third == '.')
            {
                third = '_';
            }
            char[] chars = {Character.toLowerCase(first),Character.toLowerCase(second),Character.toLowerCase(third)};
            random = new String(chars);
            return random.concat(RandomStringUtils.randomAlphanumeric(5));
        }
        return RandomStringUtils.randomAlphanumeric(8);            
    }  
    
    /**
     * Finds a local, non-loopback, IPv4 address
     * 
     * @return The first non-loopback IPv4 address found, or
     *         <code>null</code> if no such addresses found
     * @throws SocketException
     *            If there was a problem querying the network
     *            interfaces
     */
    public static InetAddress getLocalAddress() throws SocketException
    {
      Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces();
      while( ifaces.hasMoreElements() )
      {
        NetworkInterface iface = ifaces.nextElement();
        Enumeration<InetAddress> addresses = iface.getInetAddresses();

        while( addresses.hasMoreElements() )
        {
          InetAddress addr = addresses.nextElement();
          if( addr instanceof Inet4Address && !addr.isLoopbackAddress() )
          {
            return addr;
          }
        }
      }

      return null;
    }    
    
    public static long roundUp(long dividend, long divisor) {
        return (dividend + divisor - 1) / divisor;
    }  
    
    public static double roundHalfUp(double value, int places)
    {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }    
    
    /**
     * Writes given input steam to byte array.
     * @param inputStream 
     * @return  
     */
    public static byte[] toByteArray(InputStream inputStream)
    {    
        byte[] bytes = null;
        try {
            bytes = com.google.common.io.ByteStreams.toByteArray(inputStream);
        } catch (IOException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return bytes;
    }       
    
    /**
     * Reads given byte array and creates  a ByteArrayInputStream object from it.
     * @param data
     * @return 
     */
    public static InputStream toByteArrayInputStream(byte[] data)
    {    
        InputStream is = new ByteArrayInputStream(data);
        return is;
    }   
    
    public static byte[] toOutputStream(InputStream inputStream, String outputFile) throws FileNotFoundException, IOException
    {
        OutputStream outputStream;
        outputStream = new FileOutputStream(outputFile);            
        // Use an 8K data buffer
        byte[] buffer = new byte[8192];
        int count;
        while ((count = inputStream.read(buffer)) > 0)
        {
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        return buffer;         
    }
    
}
