package com.mobiweb.bucket.rs.resources;

import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.common.Utilities;
import com.mobiweb.bucket.global.WebAppContext;
import com.mobiweb.bucket.exceptions.BucketStorageException;
import com.mobiweb.bucket.exceptions.BucketExistsException;
import com.mobiweb.bucket.exceptions.UnknownBucketException;
import com.mobiweb.bucket.security.Credentials;
import com.mobiweb.bucket.security.ICredentialsService;
import com.mobiweb.bucket.security.SecretKeyException;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * RESTFul Web Services endpoint to support the handling and the encryption/decryption of 
 * file resources (bucket items) stored in bucket-type folders located in underlying server file system.
 * @author ic
 */
@Path("/bucket")
@Singleton
public class BucketResource implements IBucketRs
{    
    // private fields: server upload folder, etc.
    private static String FILE_STORE_PATH;
    private static String CATALINA_BASE_DIR;    

    private @Context ServletContext servletContext; 
    private static final Logger LOGGER = Logger.getLogger(BucketResource.class.getName());    

    //@EJB
    //private CredentialsService credentialsClient;    
    @EJB
    private ICredentialsService credentialsService;        
    
    public BucketResource()
    {        
        FILE_STORE_PATH = WebAppContext.Params.STORAGE_PATH;
        CATALINA_BASE_DIR = System.getProperty("catalina.base");
    }
    
    /**
     * Creates bucket storage folder under given name.
     * @param name
     * @param secretAccessKey
     * @param accessKeyId
     * @return 
     */
    @POST         
    @Path("/")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)    
    @Override
    public Response createBucket(@FormParam("name") String name,  
            @FormParam("secretAccessKey") String secretAccessKey, 
            @FormParam("accessKeyId") String accessKeyId)
    {
        try
        {   
            //storageService.create(name);
            LOGGER.log(Level.INFO, "Creating new bucket : {0}", name);  
            Credentials credentials = new Credentials(secretAccessKey, accessKeyId);
            credentialsService.setCredentials(credentials);            
            credentialsService.createBucket(name);                
            return Response.status(Response.Status.OK).build();
        }
        catch(EncryptionException | BucketStorageException | SecretKeyException | IOException ex)
        {
            LOGGER.log(Level.SEVERE, ex.getMessage(), name);
            throw new WebApplicationException(ex, Response.Status.BAD_REQUEST); //400            
        }
        catch (BucketExistsException ex)
        {
            LOGGER.log(Level.SEVERE, ex.getMessage(), name);
            throw new WebApplicationException(ex, Response.Status.NO_CONTENT); // 204             
        }
    }   
    
    /**
     * Deletes existing bucket item.
     * @param bucketName
     * @param httpServletRequest
     * @param secretAccessKey
     * @param accessKeyId
     * @return 
     */
    @DELETE
    @Path("/{bucketName}")      
    @Override
    public Response deleteBucket(@PathParam("bucketName") String bucketName, @Context HttpServletRequest httpServletRequest,
        @FormParam("secretAccessKey") String secretAccessKey,  @FormParam("accessKeyId") String accessKeyId)
    {
        try
        {          
            String passphrase = httpServletRequest.getHeader("passphrase");                        
            if(Utilities.isEmpty(bucketName) || Utilities.isEmpty(passphrase))
            {
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }        

            // try to delete bucket and its contents            
            //storageService.delete(storageService.load(bucketName));
            Credentials credentials = new Credentials(secretAccessKey, accessKeyId);
            credentialsService.setCredentials(credentials);            
            credentialsService.deleteBucket(bucketName);
            LOGGER.log(Level.INFO, "Bucket deleted... {0}",bucketName);
            return Response.ok().build();            
        }
        catch(EncryptionException | BucketStorageException | SecretKeyException | IOException ex)
        {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            //throw new WebApplicationException(ex, Response.Status.BAD_REQUEST);
            throw new WebApplicationException(ex, Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).type(MediaType.TEXT_HTML).build());
        }        
        catch (UnknownBucketException ex)
        {
            LOGGER.log(Level.SEVERE, ex.getMessage(), bucketName);
            throw new WebApplicationException(ex, Response.Status.NOT_FOUND); // 404 
        } 
        
        
    }

     /**
     * Updates and encrypts existing bucket item with given attached item.
     * @param bucketName
     * @param httpServletRequest
     * @return
     */
    @POST
    @Path("/{bucketName}/")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)     
    @Override
    public Response createBucketFile(@PathParam("bucketName") String bucketName, @Context HttpServletRequest httpServletRequest)
    {
        try
        {        
            // multiparts variables
            FileItem fileItem = null;        
            String passphrase = "";       
            String secretAccessKey = "";
            String accessKeyId = "";
            
            // cache-copy parts
            List<FileItem> fileItems = new ArrayList<>();
            
            // Process multipart form data
            if(ServletFileUpload.isMultipartContent(httpServletRequest))
            {
                // Set body encoding for file names etc.
                httpServletRequest.setCharacterEncoding("UTF-8"); 

                // Get parts
                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(httpServletRequest);                                

                for (FileItem item : multiparts) 
                {
                    if(item.isFormField())
                    {
                        String fieldName = item.getFieldName();
                        if(!"".equals(fieldName) && fieldName.equals("passphrase"))
                        {            
                            passphrase = item.getString();
                            //LOGGER.log(Level.INFO, "Setting pass phrase: {0}", passphrase);
                        }
                        else if(!"".equals(fieldName) && fieldName.equals("secretAccessKey"))
                        {            
                            secretAccessKey = item.getString();
                            //LOGGER.log(Level.INFO, "Setting secretAccessKey: {0}", secretAccessKey);
                        }
                        else if(!"".equals(fieldName) && fieldName.equals("accessKeyId"))
                        {            
                            accessKeyId = item.getString();
                            //LOGGER.log(Level.INFO, "Setting accessKeyId: {0}", accessKeyId);
                        }
                    }
                    else
                    {
                        String fieldName = item.getFieldName();
                        if(!"".equals(fieldName) && fieldName.equals("file"))
                        {                            
                            fileItem = item;
                            fileItems.add(item); // copy file item
                            LOGGER.log(Level.INFO, "Setting file item: {0}", fileItem.getName());
                        }
                    }
                }
            }        
            // Encrypt and write file to server's bucket storage folder            
            //LOGGER.log(Level.INFO, "Setting passphrase : {0}", passphrase);                     
            Credentials credentials = new Credentials(secretAccessKey, accessKeyId);
            credentialsService.setCredentials(credentials);            
            //credentialsService.putBucketFile(fileItem, bucketName);   
            credentialsService.putBucketFiles(fileItems, bucketName);
            
            return Response.status(Response.Status.OK).entity("File uploaded/updated successfully.").build();
        }
        catch(EncryptionException | BucketStorageException | SecretKeyException | IOException |  FileUploadException ex)
        {
            throw new WebApplicationException(ex, Response.Status.BAD_REQUEST);  
        } 
        catch (UnknownBucketException ex)
        {
            LOGGER.log(Level.SEVERE, ex.getMessage(), bucketName);
            throw new WebApplicationException(ex, Response.Status.NOT_FOUND); // 404 
        }         
    }
    
    /**
     * Downloads and decrypts existing bucket item endpoint.
     * @param bucketName
     * @param filename
     * @param httpServletRequest
     * @return 
     */
    @GET
    @Path("/{bucketName}/{filename}")  
    @Produces(MediaType.APPLICATION_OCTET_STREAM + "; charset=utf-8")
    @Override
    public Response getBucketFile(@PathParam("bucketName") String bucketName, @PathParam("filename") String filename,
            @Context HttpServletRequest httpServletRequest)
    {        
        try
        {            
            String passphrase = httpServletRequest.getHeader("passphrase");                        
            if(Utilities.isEmpty(passphrase))
            {
                LOGGER.log(Level.SEVERE, "Passphrase is null :{0}", this);
                throw new WebApplicationException(new BucketStorageException("Passphrase is null"), Response.Status.BAD_REQUEST);
            }
            String secretAccessKey = httpServletRequest.getHeader("secretAccessKey");
            if(Utilities.isEmpty(secretAccessKey))
            {
                LOGGER.log(Level.SEVERE, "secretAccessKey is null :{0}", this);
                throw new WebApplicationException(new BucketStorageException("secretAccessKey is null"), Response.Status.BAD_REQUEST);
            }            
            String accessKeyId = httpServletRequest.getHeader("accessKeyId");   
            if(Utilities.isEmpty(accessKeyId))
            {
                LOGGER.log(Level.SEVERE, "accessKeyId is null :{0}", this);
                throw new WebApplicationException(new BucketStorageException("accessKeyId is null"), Response.Status.BAD_REQUEST);
            }                        

            // fetch decrypted data          
            //byte[] data = storageService.fetch(bucketName, filename, passphrase);
            Credentials credentials = new Credentials(secretAccessKey, accessKeyId);
            credentialsService.setCredentials(credentials);
            byte[] data = credentialsService.getBucketFile(bucketName, filename);
            
            String temp = filename;
            StreamingOutput fileStream = new StreamingOutput()
            {
                @Override
                public void write(java.io.OutputStream output) throws IOException, WebApplicationException
                {         
                    output.write(data);
                    output.flush();                        
                    LOGGER.log(Level.INFO, "Decrypting file... {0}", temp);
                }
            };
            // try encode filename
            filename = URLEncoder.encode(filename, "UTF-8");
            LOGGER.log(Level.INFO, "Downloading file... {0}", temp);
            return Response.ok(fileStream,MediaType.APPLICATION_OCTET_STREAM).header("Content-Disposition", "attachment; filename=" + filename).build();            
        }
        catch(EncryptionException | BucketStorageException | SecretKeyException | IOException | ClassNotFoundException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | WebApplicationException ex)
        {            
            //throw new WebApplicationException(ex, Response.Status.BAD_REQUEST);
            throw new WebApplicationException(ex, Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).type(MediaType.TEXT_HTML).build());
        } 
        catch (UnknownBucketException ex)
        {
            LOGGER.log(Level.SEVERE, ex.getMessage(), bucketName);
            throw new WebApplicationException(ex, Response.Status.NOT_FOUND); // 404 
        }         
        
    }
    
    /**
     * Deletes existing bucket item.
     * @param bucketName
     * @param filename
     * @param secretAccessKey
     * @param accessKeyId
     * @param httpServletRequest
     * @return 
     */
    @DELETE
    @Path("/{bucketName}/{filename}")   
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Override
    public Response deleteBucketFile(@PathParam("bucketName") String bucketName, @PathParam("filename") String filename,
            @FormParam("secretAccessKey") String secretAccessKey, @FormParam("accessKeyId") String accessKeyId, 
            @Context HttpServletRequest httpServletRequest)
    {
        try
        {            
            // Do some checking
            if(Utilities.isEmpty(secretAccessKey))
            {
                LOGGER.log(Level.SEVERE, "secretAccessKey is null :{0}", this);
                throw new WebApplicationException(new BucketStorageException("secretAccessKey is null"), Response.Status.BAD_REQUEST);
            }              
            if(Utilities.isEmpty(accessKeyId))
            {
                LOGGER.log(Level.SEVERE, "accessKeyId is null :{0}", this);
                throw new WebApplicationException(new BucketStorageException("accessKeyId is null"), Response.Status.BAD_REQUEST);
            }        
            Credentials credentials = new Credentials(secretAccessKey, accessKeyId);
            credentialsService.setCredentials(credentials);  
            credentialsService.deleteBucketFile(bucketName, filename);
            return Response.ok("File deleted.").build();            
        }
        catch(EncryptionException | BucketStorageException | SecretKeyException | IOException | WebApplicationException ex)
        {
            //throw new WebApplicationException(ex, Response.Status.BAD_REQUEST);
            throw new WebApplicationException(ex, Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).type(MediaType.TEXT_HTML).build());
        }     
        catch (UnknownBucketException ex)
        {
            LOGGER.log(Level.SEVERE, ex.getMessage(), bucketName);
            throw new WebApplicationException(ex, Response.Status.NOT_FOUND); // 404 
        }         
    }    
}
