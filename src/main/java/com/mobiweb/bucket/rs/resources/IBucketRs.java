package com.mobiweb.bucket.rs.resources;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

/**
 * Defines restful endpoint service methods.
 * @author ic
 */
public interface IBucketRs
{
    public Response createBucket(String name, String secretAccessKey, String accessKeyId);
    
    public Response deleteBucket(String bucketName, HttpServletRequest httpServletRequest, String secretAccessKey, String accessKeyId);
    
    public Response createBucketFile(String bucketName, HttpServletRequest httpServletRequest);
    
    public Response getBucketFile(String bucketName, String filename, HttpServletRequest httpServletRequest);
    
    public Response deleteBucketFile(String bucketName, String filename, String secretAccessKey, String accessKeyId, HttpServletRequest httpServletRequest);    
    
}
