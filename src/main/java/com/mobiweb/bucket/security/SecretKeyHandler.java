/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.security;

import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.cryptography.Cryptographic;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains methods to handle and support secret key storage.
 * @author ic
 */
@Singleton
@LocalBean
public class SecretKeyHandler
{
    private static final String CATALINA_BASE = System.getProperty("catalina.base");       
    private static final Logger LOGGER = LoggerFactory.getLogger(SecretKeyHandler.class);  
    private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(SecretKeyHandler.class.getName());  
    private final KeyStore keyStore;    
    
    @Resource(name="keystoreFilePath")
    private String keystoreFilePath;
    
    @Resource(name="keystorePassword")
    private String keystorePassword;   


    public SecretKeyHandler() throws KeyStoreException, EncryptionException
    {
        // Use cryptographic algorithm for Secret Key storage
        this.keyStore = KeyStore.getInstance("JCEKS");        
    }

    public SecretKey getKey(String alias, String password) throws SecretKeyException
    {        
        try
        {         
            String fullpath = CATALINA_BASE.concat(keystoreFilePath);    
            createKeyStoreFile(fullpath);
            keyStore.load(new FileInputStream(fullpath), keystorePassword.toCharArray());
            boolean containsAlias = keyStore.containsAlias(alias);            
            logger.log(Level.INFO, String.valueOf(containsAlias), containsAlias);
            if(existsAccessKey(alias))
            {
                Key key = keyStore.getKey(alias, password.toCharArray());   
                LOGGER.debug("Key bytes encoded..." + String.valueOf(String.valueOf(key.getEncoded())), key);
                logger.log(Level.INFO, "Key bytes encoded..." + String.valueOf(String.valueOf(key.getEncoded())), key);
                return (SecretKey) key;        
            }
            else
            {
                SecretKey sk = this.newKey(password);
                this.storeKey(sk, alias, password);
                LOGGER.debug("New key encoded..." + String.valueOf(String.valueOf(sk.getEncoded())), sk);
                logger.log(Level.INFO, "New key encoded..." + String.valueOf(String.valueOf(sk.getEncoded())), sk);
                return sk;
            }
        }
        catch (IOException | KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException | CertificateException ex)
        {
            throw new SecretKeyException(ex.getMessage(),ex);
        }
    }
    
    protected boolean createKeyStoreFile(String fullpath) throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException
    {
        File file = new File(fullpath);
        if(!file.exists())
        {
            keyStore.load(null, "".toCharArray());
            keyStore.store(new FileOutputStream(file.getAbsolutePath()), keystorePassword.toCharArray());
            logger.log(Level.INFO, "Creating keystore file...", file.getAbsolutePath());
            return true;
        }        
        return false;
    }
    
    public void storeKey(SecretKey secretKey, String alias, String password) throws SecretKeyException
    {               
        try
        {
            String fullpath = CATALINA_BASE.concat(keystoreFilePath);      
            createKeyStoreFile(fullpath);
            keyStore.load(new FileInputStream(fullpath), keystorePassword.toCharArray());
            boolean containsAlias = keyStore.containsAlias(alias);               
            LOGGER.debug(String.valueOf(containsAlias), containsAlias);
            keyStore.setKeyEntry(alias, secretKey, password.toCharArray(), null); 
            containsAlias = keyStore.containsAlias(alias);   
            LOGGER.debug(String.valueOf(containsAlias), containsAlias);
            keyStore.store(new FileOutputStream(fullpath), keystorePassword.toCharArray());      
        }
        catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException ex)
        {
            throw new SecretKeyException(ex.getMessage(),ex);
        }                
    }    
    
    private boolean existsAccessKey(String accessKeyId) throws SecretKeyException
    {
        try
        {
            String fullpath = CATALINA_BASE.concat(keystoreFilePath);
            keyStore.load(new FileInputStream(fullpath), keystorePassword.toCharArray());
            return keyStore.isKeyEntry(accessKeyId);
        }
        catch(IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException ex)
        {
            throw new SecretKeyException(ex.getMessage(),ex); 
        }
    }

    private SecretKey newKey(String password) throws SecretKeyException
    {
        try
        {
            return Cryptographic.generateSecretKey(password);
        }
        catch(NoSuchAlgorithmException | InvalidKeySpecException ex)
        {
            throw new SecretKeyException(ex.getMessage(),ex); 
        }        
    }    
    
   public SecretKey newKey(String alias, String password) throws SecretKeyException
    {
        try
        {
            SecretKey sk = this.newKey(password);
            boolean containsAlias = keyStore.containsAlias(alias); 
            if(containsAlias){
                throw new SecretKeyException("Alias name exists");
            }
            this.storeKey(sk, alias, password);            
            return sk;
        }
        catch(KeyStoreException ex)
        {
            throw new SecretKeyException(ex.getMessage(),ex); 
        }        
    }   
    
}
