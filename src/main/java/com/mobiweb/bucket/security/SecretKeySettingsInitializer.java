/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.security;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Initializes and creates settings for secret key storage.
 * @author ic
 */
@Singleton
@LocalBean
public class SecretKeySettingsInitializer
{
    private static final String CATALINA_BASE = System.getProperty("catalina.base");       
    private static final Logger logger = LoggerFactory.getLogger(SecretKeySettingsInitializer.class);   
    private KeyStore keyStore;
    @Resource(name="keystoreFilePath")
    private String keystoreFilePath;    
    @Resource(name="keystorePassword")
    private String keystorePassword;

    public SecretKeySettingsInitializer() throws SecretKeyInitializationException
    {        
    }
    
    /**
     * Initializes and creates the KeyStore file in server file system.
     * @throws com.mobiweb.bucket.security.SecretKeyInitializationException 
     */    
    public void init() throws SecretKeyInitializationException
    {
        try
        {
            // Initialize keystore object for SecretKey usage
            keyStore = KeyStore.getInstance("JCEKS");
            String fullpath = CATALINA_BASE.concat(keystoreFilePath);
            File file = new File(fullpath);            
            if(!file.exists())
            {    
                keyStore.load(null, "".toCharArray());
                keyStore.store(new FileOutputStream(file.getAbsolutePath()), keystorePassword.toCharArray());
                logger.info("Creating keystore file...", file.getAbsolutePath());
            }            
        }
        catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException ex)
        {
            throw new SecretKeyInitializationException(ex.getMessage(), ex);
        }
    }   
    
}
