/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.security;

import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.exceptions.BucketExistsException;
import com.mobiweb.bucket.exceptions.BucketStorageException;
import com.mobiweb.bucket.exceptions.UnknownBucketException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.ejb.Local;
import org.apache.commons.fileupload.FileItem;

/**
 *
 * @author ic
 */
@Local
public interface ICredentialsService
{
    public void setCredentials(Credentials credentials) throws SecretKeyException;
    
    public void createBucket(String bucketName) throws BucketStorageException, IOException, EncryptionException, UnsupportedEncodingException, BucketExistsException;
    
    public void deleteBucket(String bucketName) throws BucketStorageException, IOException, EncryptionException, UnknownBucketException;
    
    public void putBucketFile(FileItem fileItem, String bucketName) throws BucketStorageException, EncryptionException, UnknownBucketException, IOException;
    
    public void deleteBucketFile(String bucketName, String fileName) throws BucketStorageException, IOException, EncryptionException, UnknownBucketException;
    
    public byte[] getBucketFile(String bucketName, String fileName) throws BucketStorageException, EncryptionException, IOException, IllegalBlockSizeException, ClassNotFoundException, BadPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnknownBucketException;
    
    public void putBucketFiles(List<FileItem> fileItems, String bucketName) throws EncryptionException, UnknownBucketException, BucketStorageException, UnsupportedEncodingException, IOException;
}
