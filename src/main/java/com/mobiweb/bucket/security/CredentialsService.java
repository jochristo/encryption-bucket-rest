package com.mobiweb.bucket.security;

import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.exceptions.BucketStorageException;
import com.mobiweb.bucket.exceptions.BucketExistsException;
import com.mobiweb.bucket.exceptions.UnknownBucketException;
import com.mobiweb.bucket.core.Bucket;
import com.mobiweb.bucket.core.UploadBucketFileItem;
import com.mobiweb.bucket.service.BucketService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import javax.crypto.SecretKey;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import org.apache.commons.fileupload.FileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 

/**
 * Credentials broker service to support bucket and bucket files operations.
 * Handles all RESTful HTTP requests to and from service endpoints.
 * Invokes service beans to support secret key handling, storage, and encryption/decryption of data files.
 * 
 * @author ic
 */
@Singleton
@LocalBean
public class CredentialsService implements ICredentialsService
{
    private SecretKey secretKey;
    private String keyPath;    
    private String secretAccessKey;
    private String accessKeyId;        
    private Credentials credentials;
    private CredentialType credentialType;
    @EJB private SecretKeyHandler secretKeyHandler;           
    @EJB private BucketService bucketService;
    private static final Logger logger = LoggerFactory.getLogger(CredentialsService.class); 
    
    public CredentialsService() {        
    }

    public CredentialsService(String accessKeyId, String secretAccessKey) throws SecretKeyException
    {
        this.secretAccessKey = secretAccessKey;
        this.accessKeyId = accessKeyId;
        // use id as alias for keystore and access key as password - to create, store and load SecreKey object
        this.secretKey = secretKeyHandler.getKey(accessKeyId, secretAccessKey);
        this.credentials = new Credentials(secretAccessKey, accessKeyId);
    }

    public CredentialsService(Credentials credentials) throws SecretKeyException
    {
        // use id as alias for keystore and access key as password - to create, store and load SecreKey object
        this.credentials = credentials;
        this.secretKey = secretKeyHandler.getKey(credentials.getAccessKeyId(), credentials.getSecretAccessKey()); 
        logger.info("Creating new credentials with key...", credentials.getSecretAccessKey());
    }   
    
    public CredentialsService(Credentials credentials, CredentialType credentialType) throws SecretKeyException
    {
        // use id as alias for keystore and access key as password - to create, store and load SecreKey object
        this.credentials = credentials;
        this.credentialType = credentialType;
        if(credentialType == CredentialType.CREATE_BUCKET)
        {
            // fetch new key only
            this.secretKey = secretKeyHandler.newKey(credentials.getAccessKeyId(), credentials.getSecretAccessKey());                    
        }
        else
        {
            this.secretKey = secretKeyHandler.getKey(credentials.getAccessKeyId(), credentials.getSecretAccessKey());
        }
    }    

    public CredentialType getCredentialType() {
        return credentialType;
    }

    public void setCredentialType(CredentialType credentialType) {
        this.credentialType = credentialType;
    }    
    
    public Credentials getCredentials() {
        return credentials;
    }

    @Override
    public void setCredentials(Credentials credentials) throws SecretKeyException {
        this.credentials = credentials;
        this.secretKey = secretKeyHandler.getKey(credentials.getAccessKeyId(), credentials.getSecretAccessKey()); 
    }   
        
    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getSecretAccessKey() {
        return secretAccessKey;
    }

    public void setSecretAccessKey(String secretAccessKey) {
        this.secretAccessKey = secretAccessKey;
    }    
    
    public SecretKey getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(SecretKey secretKey) {
        this.secretKey = secretKey;
    }

    public String getKeyPath() {
        return keyPath;
    }

    public void setKeyPath(String keyPath) {
        this.keyPath = keyPath;
    }
    
    @Override
    public void createBucket(String bucketName) throws BucketStorageException, IOException, EncryptionException, UnsupportedEncodingException, BucketExistsException
    {
        bucketService.create(bucketName, secretKey);
    }
    
    @Override
    public void deleteBucket(String bucketName) throws BucketStorageException, IOException, UnknownBucketException
    {
        bucketService.delete(bucketName, secretKey);
    }     
    
    @Override
    public void putBucketFile(FileItem fileItem, String bucketName) throws BucketStorageException, EncryptionException, UnknownBucketException, IOException
    {
        Bucket bucket = bucketService.load(bucketName, secretKey);
        bucketService.createFile(bucket, new UploadBucketFileItem(fileItem), secretKey);
    }
    
    @Override
    public void putBucketFiles(List<FileItem> fileItems, String bucketName) throws EncryptionException, UnknownBucketException, BucketStorageException, UnsupportedEncodingException, IOException
    {
        Bucket bucket = bucketService.load(bucketName, secretKey);
        fileItems.forEach((item)->
        {
            try {         
                bucketService.createFile(bucket, new UploadBucketFileItem(item), secretKey);
            } catch (BucketStorageException | EncryptionException ex) {
                java.util.logging.Logger.getLogger(CredentialsService.class.getName()).log(Level.SEVERE, null, ex);                
            }
        });

    }    
    
    @Override
    public void deleteBucketFile(String bucketName, String fileName) throws BucketStorageException, IOException, EncryptionException, UnknownBucketException
    {
        Bucket bucket = bucketService.load(bucketName, secretKey);
        bucketService.deleteFile(bucket, fileName, secretKey);
    }    
    
    @Override
    public byte[] getBucketFile(String bucketName, String fileName) throws BucketStorageException, UnknownBucketException, IOException
    {
        Bucket bucket = bucketService.load(bucketName, secretKey);
        return bucketService.getBucketFile(bucket, fileName, secretKey).getContents();
    }
    
  
    
}
