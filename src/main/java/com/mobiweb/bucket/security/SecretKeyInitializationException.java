/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.security;

/**
 * Exception thrown during secret key initialization.
 * @author Admin
 */
public class SecretKeyInitializationException extends Exception
{

    public SecretKeyInitializationException(String message) {
        super(message);
    }

    public SecretKeyInitializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecretKeyInitializationException(Throwable cause) {
        super(cause);
    }

}
