
package com.mobiweb.bucket.security;

    /**
     * Exception thrown during secret key initialization.
     */
    public class SecretKeyException extends Exception
    {

        public SecretKeyException(String message) {
            super(message);
        }

        public SecretKeyException(String message, Throwable cause) {
            super(message, cause);
        }

        public SecretKeyException(Throwable cause) {
            super(cause);
        }
        
    } 
