/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.security;

/**
 * Supports the type of credentials requested.
 * @author ic
 */
public enum CredentialType
{
    CREATE_BUCKET, 
    DELETE_BUCKET , 
    GET_FILE, 
    UPDATE_FILE, 
    DELETE_FILE;
}
