package com.mobiweb.bucket.security;

import java.util.UUID;

/**
 * Represent credentials obtained with secret access key and access key id.
 * @author ic
 */
public class Credentials
{
    private String secretAccessKey;
    private String accessKeyId;

    public Credentials(String secretAccessKey, String accessKeyId) {
        this.secretAccessKey = secretAccessKey;
        this.accessKeyId = accessKeyId;
    }

    public Credentials(String secretAccessKey) {
        this.secretAccessKey = secretAccessKey;
        this.accessKeyId = UUID.randomUUID().toString();
    }    
    
    public String getSecretAccessKey() {
        return secretAccessKey;
    }

    public void setSecretAccessKey(String secretAccessKey) {
        this.secretAccessKey = secretAccessKey;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }
    
    
    
}
