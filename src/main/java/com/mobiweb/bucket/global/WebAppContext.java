/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.global;

/**
 * Contains global methods, and properties to access from this web application context.
 * @author ic
 */
public abstract class WebAppContext
{
    public static class Params
    {
        public static final String STORAGE_PATH = "Bucket.Storage.Path";
        public static final String STORAGE_SETTINGS_FILE = "Bucket.Storage.Settings.File";  
        public static final String STORAGE_SETTINGS_PATH = "Bucket.Storage.Settings.Path"; 
        public static final String STORAGE_KEYSTORE_PATH = "Bucket.Storage.Keystore"; 
    } 
   

}
