/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.exceptions;

/**
 *
 * @author Admin
 */
public class BucketExistsException extends Exception
{

    public BucketExistsException(String message) {
        super(message);
    }

    public BucketExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public BucketExistsException(Throwable cause) {
        super(cause);
    }
    
}
