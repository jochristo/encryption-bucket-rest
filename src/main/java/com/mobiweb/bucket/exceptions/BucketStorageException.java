package com.mobiweb.bucket.exceptions;

/**
 * General exception than can be thrown during handling operations on 
 * {@link com.mobiweb.bucket.storage.BucketStorage} 
 * and {@link com.mobiweb.bucket.storage.BucketItem} instances.
 * @author ic
 */
public class BucketStorageException extends Exception
{

    public BucketStorageException(String message) {
        super(message);
    }

    public BucketStorageException(String message, Throwable cause) {
        super(message, cause);
    }

    public BucketStorageException(Throwable cause) {
        super(cause);
    }
    
}
