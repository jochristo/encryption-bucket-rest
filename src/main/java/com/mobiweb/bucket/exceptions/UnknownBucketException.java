/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.exceptions;

/**
 *
 * @author Admin
 */
public class UnknownBucketException extends Exception
{

    public UnknownBucketException(String message) {
        super(message);
    }

    public UnknownBucketException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownBucketException(Throwable cause) {
        super(cause);
    }
    
}
