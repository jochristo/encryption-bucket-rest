package com.mobiweb.bucket.startup;

import com.mobiweb.bucket.security.SecretKeyInitializationException;
import com.mobiweb.bucket.security.SecretKeySettingsInitializer;
import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 

/**
 * Startup singleton to initialize application components.
 * @author ic
 */
@Singleton
@Startup
@DependsOn({"SecretKeySettingsInitializer"})
public class Init
{   
    private static final Logger logger = LoggerFactory.getLogger(Init.class);  
    @EJB SecretKeySettingsInitializer secretKeySettingsInitializer;
        
    @PostConstruct    
    private void postConstruct()
    {
        try
        {
            secretKeySettingsInitializer.init();
            logger.debug("Started SecretKeySettingsInitializer...", secretKeySettingsInitializer); 
            logger.info("Started SecretKeySettingsInitializer...", secretKeySettingsInitializer);
        }
        catch (SecretKeyInitializationException ex)
        {            
            logger.error(ex.getMessage(), ex);
        }        
    }
    
    
    
}
