/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.core;

/**
 * Represents a readable file system object using an absolute file path.
 * @author ic
 */
public interface IReadable
{
    public void setSourcePath(String absoluteFilePath);
    
    public String getSourcePath();
    
    public String getFilename();    
}
