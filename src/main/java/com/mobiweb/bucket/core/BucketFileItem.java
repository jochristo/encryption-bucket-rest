package com.mobiweb.bucket.core;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;

/**
 * Base bucket file item class.
 * @author ic
 */
public class BucketFileItem
{
    private String filename;
    private Object contents;
    private String extension;
    private FileItem fileItem;
    
    public BucketFileItem(Object data, String filename) {
        this.filename = filename;
        this.contents = data;
        this.extension = FilenameUtils.getExtension(filename);
    }

    public BucketFileItem(FileItem fileItem) {
        this.fileItem = fileItem;
        this.extension = FilenameUtils.getExtension(this.fileItem.getName());
        this.filename = fileItem.getName();
        this.contents = fileItem.get();
    }     
        
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Object getContents() {
        return contents;
    }

    public void setContents(Object contents) {
        this.contents = contents;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
    
    
}
