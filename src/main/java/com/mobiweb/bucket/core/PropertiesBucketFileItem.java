/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.core;

/**
 * Bucket-properties bucket file item.
 * @author ic
 */
public class PropertiesBucketFileItem extends BucketFileItem
{
    
    public PropertiesBucketFileItem(String json, String filename) {
        super(json, filename);
    }
    
}
