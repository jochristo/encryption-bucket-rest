/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.common.Utilities;
import com.mobiweb.bucket.common.Hashing;
import com.mobiweb.bucket.exceptions.BucketStorageException;
import com.mobiweb.bucket.cryptography.Cryptographic;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.UUID;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.util.Strings;

/**
 * Represents a regular (uploaded) bucket file.
 * @author ic
 */
public class RegularBucketFile extends BaseBucketFile implements IAttributes
{
    @Expose
    private String serviceRelativePath;
    
    @Expose
    private String encodedData;
   
    public String getEncodedData() {
        return encodedData;
    }

    public void setEncodedData(String encodedData) {
        this.encodedData = encodedData;
    }    

    public String getServiceRelativePath() {
        return serviceRelativePath;
    }

    public void setServiceRelativePath(String serviceRelativePath) {
        this.serviceRelativePath = serviceRelativePath;
    }    
    
    public RegularBucketFile(String originalFilename, Bucket bucket) {
        super(originalFilename, bucket);        
        super.setOriginalFilename("");            
        super.setFilename(originalFilename);            
        super.setRelativePath(Utilities.createPathFromName(super.getBucketName()).concat(super.getBucketName()).concat(File.separator).concat(super.getFilename()));
        super.setId(Hashing.hash(originalFilename));                
        this.serviceRelativePath = File.separator + bucket.getName() + File.separator + super.getFilename();
        super.setBucket(bucket);        
    }
    
    public RegularBucketFile(String originalFilename, Bucket bucket, boolean isPropertiesFileRecreated) {
        super(originalFilename, bucket);
        if(isPropertiesFileRecreated)
        {
            super.setOriginalFilename("");            
            super.setFilename(originalFilename);            
            super.setRelativePath(Utilities.createPathFromName(super.getBucketName()).concat(super.getBucketName()).concat(File.separator).concat(super.getFilename()));
            super.setId(Hashing.hash(super.getFilename()));
        }
        this.serviceRelativePath = File.separator + bucket.getName() + File.separator + super.getFilename();
        super.setBucket(bucket);
    }
    
    public RegularBucketFile(UploadBucketFileItem bucketFileItem, Bucket bucket) throws IOException {
        super(bucketFileItem, bucket);
        hashName();
        
        // set its name MD5 hash as id
        super.setId(Hashing.hash(super.getFilename()));
        this.serviceRelativePath = File.separator + bucket.getName() + File.separator + super.getFilename();
        super.setBucket(bucket);
    }
    
    public RegularBucketFile(BucketFileItem bucketFileItem, Bucket bucket) {
        super(bucketFileItem, bucket);
        hashName();
        
        // set its name MD5 hash as id
        super.setId(Hashing.hash(super.getFilename()));
        this.serviceRelativePath = File.separator + bucket.getName() + File.separator + super.getFilename();
        super.setBucket(bucket);
    }    

    public RegularBucketFile() {
    }
    
    /**
     * Clear encoded data property value. 
     * @return 
     */
    public RegularBucketFile clearData()
    {
        this.setEncodedData(null);
        return this;
    }
    
    private void hashName()
    {
        String extension = FilenameUtils.getExtension(this.getOriginalFilename());
        String copy = this.getOriginalFilename();        
        this.setFilename(UUID.randomUUID().toString()); // use UUID instead
        if(!Strings.isBlank(extension))
        {
            this.setFilename(UUID.randomUUID().toString().concat(".").concat(extension)); // use UUID instead
            this.setRelativePath(Utilities.createPathFromName(getBucketName()).concat(getBucketName()).concat(File.separator).concat(getFilename()));
        }         
    }    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegularBucketFile other = (RegularBucketFile) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    @Override
    public RegularBucketFile append(BasicFileAttributes attrs) {
        LocalDateTime ldt = LocalDateTime.ofInstant(attrs.creationTime().toInstant(),ZoneId.systemDefault());
        this.setCreatedAt(ldt.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));
        return this;
    }   
    
    @Override
    public RegularBucketFile encrypted(Cryptographic cryptographic) throws BucketStorageException
    {
        try
        {            
            // encode data as json string
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create(); 
            String base64Contents = DatatypeConverter.printBase64Binary(this.getContents());
            this.setEncodedData(base64Contents);
            String json = gson.toJson(this, RegularBucketFile.class);            
            
            String absoluteFilePath = new File(this.getBucket().getAbsolutePath().concat(File.separator).concat(this.getFilename())).getAbsolutePath();
            Encrypted encrypted = new Encrypted(json.getBytes("UTF-8"), absoluteFilePath );            
            byte[] encrdata = encrypted.transformed(cryptographic, cryptographic.getSecretKey());
            Cryptographic.write(encrdata, encrypted.getAbsoluteFilePath(), false);

            // update bytes with encrypted data
            this.setContents(encrdata);
            this.clearData();
            return this;
        }
        catch(EncryptionException | IOException ex)
        {
            throw new BucketStorageException(ex.getMessage(), ex);
        }
    }

    @Override
    public RegularBucketFile decrypted(Cryptographic cryptographic) throws EncryptionException, UnsupportedEncodingException, IOException
    {
        String path = this.getBucket().getAbsolutePath() + File.separator + this.getFilename();
        File file = new File(path);                            
        Decrypted decrypted = new Decrypted(file.getAbsolutePath());
        byte[] data = decrypted.transformed(cryptographic, cryptographic.getSecretKey());        
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create(); 
        String json = new String(data,"UTF-8");
        RegularBucketFile regularBucketFile = gson.fromJson(json, RegularBucketFile.class);
        byte[] base64Decoded = DatatypeConverter.parseBase64Binary(regularBucketFile.getEncodedData());
        return new RegularBucketFile(new BucketFileItem(base64Decoded, this.getFilename()), this.getBucket());
    }

    
    
}
