package com.mobiweb.bucket.core;

import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.common.Utilities;
import com.mobiweb.bucket.cryptography.Cryptographic;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKey;

/**
 * Represents a decrypted object.
 * @author ic
 */
public class Decrypted implements ITransformation, IReadable
{
    private static final Logger logger = Logger.getLogger(Decrypted.class.getName());
    private byte[] data = null;
    private String absoluteFilePath = "";  

    public Decrypted(String absoluteFilePath)
    {
        this.absoluteFilePath = absoluteFilePath;
        this.data = Utilities.toByteArray(absoluteFilePath);
    }  
    
    /**
     *
     * @param cryptographic
     * @param secretKey
     * @throws EncryptionException
     * @throws IOException
     */
    @Override
    public void transform(Cryptographic cryptographic,SecretKey secretKey) throws EncryptionException, IOException
    {
        byte[] decrypted = cryptographic.decrypt(this.data, secretKey);
        this.data = decrypted;
        logger.log(Level.INFO, "Transforming data... ", secretKey);                    
    }
    
    /**
     *
     * @param cryptographic
     * @param secretKey
     * @return
     * @throws EncryptionException
     * @throws IOException
     */
    @Override
    public byte[] transformed(Cryptographic cryptographic,SecretKey secretKey) throws EncryptionException, IOException
    {
        byte[] decrypted = cryptographic.decrypt(this.data, secretKey);
        logger.log(Level.INFO, "Transforming data... ", secretKey);                       
        return decrypted;
    }     

    @Override
    public void setSourcePath(String absoluteFilePath) {
        this.absoluteFilePath = absoluteFilePath;
    }

    @Override
    public String getSourcePath() {
        return this.absoluteFilePath;
    }

    @Override
    public String getFilename() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
