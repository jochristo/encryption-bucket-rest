/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.core;

import java.nio.file.attribute.BasicFileAttributes;

/**
 * Appends some attributes.
 * @author ic
 */
public interface IAttributes {
    
    public BaseBucketFile append(BasicFileAttributes attrs);
}
