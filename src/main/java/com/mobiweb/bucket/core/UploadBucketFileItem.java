/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.core;

import org.apache.commons.fileupload.FileItem;

/**
 * Uploadable bucket file item.
 * @author ic
 */
public class UploadBucketFileItem extends BucketFileItem{
    
    public UploadBucketFileItem(FileItem fileItem) {
        super(fileItem);
    }
    
}
