package com.mobiweb.bucket.core;

import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.exceptions.BucketExistsException;
import com.mobiweb.bucket.exceptions.BucketStorageException;
import com.mobiweb.bucket.exceptions.UnknownBucketException;
import java.io.File;
import java.io.IOException;
import javax.crypto.SecretKey;
import javax.ejb.Local;

/**
 * Contains methods to handle buckets.
 * @author ic
 * @param <T>
 */
@Local
public interface IBucketFactory<T extends IBucket>
{
    public T create(String bucketName, SecretKey secretKey) throws BucketStorageException, IOException, BucketExistsException;
    
    public boolean delete(String bucketName, SecretKey secretKey) throws BucketStorageException, IOException, EncryptionException, UnknownBucketException;
    
    public boolean exists(String bucketName);
    
    public Bucket load(String bucketName, SecretKey secretKey)throws BucketStorageException, UnknownBucketException, IOException;
    
    public Bucket getBucketFromPropertiesFile(T bucket, File file, SecretKey secretKey) throws BucketStorageException;
    
    public void createPropertiesFile(T bucket, String storagePath, SecretKey secretKey) throws BucketStorageException;
    
    public PropertiesBucketFile createPropertiesFile(T bucket, SecretKey secretKey) throws BucketStorageException;
    
    public PropertiesBucketFile getPropertiesFile(T bucket,SecretKey secretKey);
    
    public PropertiesBucketFile updatePropertiesFile(T bucket,SecretKey secretKey) throws BucketStorageException;
    
    public boolean writePropertiesFile(Bucket bucket,SecretKey secretKey) throws BucketStorageException, IOException;
}
