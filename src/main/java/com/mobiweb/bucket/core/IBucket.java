/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.core;

import java.util.Set;

/**
 * Defines getter and setter bucket methods.
 * @author ic
 */
public interface IBucket
{
    public String getUuid();
    
    public void setUuid(String uuid);
    
    public String getName();
    
    public void setName(String name);
    
    public String getRelativePath();
    
    public void setRelativePath(String relativePath);    
    
    public String getPropertiesFile() ;
    
    public void setPropertiesFile(String propertiesFile);
    
    public Set<BaseBucketFile> getBucketFiles();
    
    public void setBucketFiles(Set<BaseBucketFile> bucketFiles);
    
    public String getAbsolutePath();
    
    public void setAbsolutePath(String absolutePath);
}
