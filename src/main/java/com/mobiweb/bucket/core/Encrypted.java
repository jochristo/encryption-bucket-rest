package com.mobiweb.bucket.core;

import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.cryptography.Cryptographic;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKey;

/**
 * Represents an encrypted object.
 * @author ic
 */
public class Encrypted implements ITransformation, IWritable
{
    private static final Logger logger = Logger.getLogger(Encrypted.class.getName());
    private String absoluteFilePath = "";
    private byte[] data = null;   

    @Override
    public void setData(byte[] data) {
        this.data = data;
    }    

    @Override
    public String getAbsoluteFilePath() {
        return absoluteFilePath;
    }

    @Override
    public void setAbsoluteFilePath(String absoluteFilePath) {
        this.absoluteFilePath = absoluteFilePath;
    }        

    public Encrypted(byte[] data, String absoluteFilePath) {
        this.absoluteFilePath = absoluteFilePath;
        this.data = data;
    }  
    
    /**
     *
     * @param cryptographic
     * @param secretKey
     * @throws EncryptionException
     * @throws IOException
     */
    @Override
    public void transform(Cryptographic cryptographic,SecretKey secretKey) throws EncryptionException, IOException
    {
        byte[] transformed = cryptographic.encrypt(this.data, secretKey);
        logger.log(Level.INFO, "Transforming data... ", secretKey);            
        Cryptographic.write(transformed, absoluteFilePath, false); // need to store absolute path  to file
        this.setData(transformed);
        logger.log(Level.INFO, "Writing data to file path -> {0}", absoluteFilePath);
    }
    
    /**
     *
     * @param cryptographic
     * @param secretKey
     * @return
     * @throws EncryptionException
     * @throws IOException
     */
    @Override
    public byte[] transformed(Cryptographic cryptographic, SecretKey secretKey) throws EncryptionException, IOException
    {
        byte[] transformed = cryptographic.encrypt(this.data, secretKey);
        logger.log(Level.INFO, "Transforming data... ", secretKey);                       
        //logger.log(Level.INFO, "Writing data to file path -> {0}", absoluteFilePath);        
        return transformed;
    }     

}
