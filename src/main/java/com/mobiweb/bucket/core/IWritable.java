/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.core;

/**
 * Represents a writable to file system object using an absolute file path.
 * @author ic
 */
public interface IWritable
{
    public String getAbsoluteFilePath();
    public void setAbsoluteFilePath(String absoluteFilePath);    
    public void setData(byte [] data);
    
}
