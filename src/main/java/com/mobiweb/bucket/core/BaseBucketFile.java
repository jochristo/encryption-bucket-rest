package com.mobiweb.bucket.core;

import com.google.gson.annotations.Expose;
import com.mobiweb.bucket.common.Utilities;
import com.mobiweb.bucket.common.Hashing;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.UUID;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.util.Strings;

/**
 * Base class for bucket files.
 * @author ic
 */
public abstract class BaseBucketFile implements IBucketFile
{
    @Expose
    private String id;     
    
    @Expose
    private String filename;    
    
    @Expose
    private String originalFilename;      
    
    @Expose
    private String relativePath;
    
    @Expose
    private String bucketUuidReference;      
    
    @Expose
    private String bucketName;  
    
    private byte[] contents;    
    
    @Expose
    private String createdAt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
    
    private Bucket bucket;
    
    @Expose
    private String storagePath;

    
    @Override
    public String getStoragePath() {
        return storagePath;
    }

    @Override
    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }    

    @Override
    public Bucket getBucket() {
        return bucket;
    }

    @Override
    public void setBucket(Bucket bucket) {
        this.bucket = bucket;
    }    

    @Override
    public String getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }       

    @Override
    public byte[] getContents() {
        return contents;
    }

    @Override
    public void setContents(byte[] contents) {
        this.contents = contents;
    }   
    
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public String getOriginalFilename() {
        return originalFilename;
    }

    @Override
    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    @Override
    public String getRelativePath() {
        return relativePath;
    }

    @Override
    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    @Override
    public String getBucketName() {
        return bucketName;
    }

    @Override
    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    @Override
    public String getBucketUuidReference() {
        return bucketUuidReference;
    }

    @Override
    public void setBucketUuidReference(String bucketUuidReference) {
        this.bucketUuidReference = bucketUuidReference;
    }

    public BaseBucketFile() {
    }    
    
    
    /**
     * Used for uploaded files.
     * @param originalFilename
     * @param bucket 
     */
    public BaseBucketFile(String originalFilename, Bucket bucket) {
        this.originalFilename = originalFilename;
        this.bucketName = bucket.getName();
        this.bucketUuidReference = bucket.getUuid();
        setProperties();
    }       
    
    /**
     * Used for bucket files other than uploaded ones, e.g. bucket properties files.
     * @param bucket
     * @param filename 
     */
    public BaseBucketFile(Bucket bucket, String filename) {
        this.originalFilename = filename;
        this.bucketName = bucket.getName();
        this.bucketUuidReference = bucket.getUuid();
        this.id = UUID.randomUUID().toString();
        this.filename = filename;
        this.relativePath = Utilities.createPathFromName(this.bucketName).concat(this.bucketName).concat(File.separator).concat(this.filename);
        this.storagePath = new File(bucket.getAbsolutePath() + File.separator + filename).getAbsolutePath();
        
    }    
    
    public BaseBucketFile(BucketFileItem bucketFileItem, Bucket bucket)
    {
        this.originalFilename = bucketFileItem.getFilename();
        this.bucketName = bucket.getName();
        this.bucketUuidReference = bucket.getUuid();
        this.id = UUID.randomUUID().toString();
        this.filename = bucketFileItem.getFilename();
        this.relativePath = Utilities.createPathFromName(this.bucketName).concat(this.bucketName).concat(File.separator).concat(this.filename);        
        this.contents = (byte[]) bucketFileItem.getContents();
    }

    private void setProperties()
    {
        this.id = UUID.randomUUID().toString();
        String extension = FilenameUtils.getExtension(this.originalFilename);
        String copy = this.originalFilename;
        this.filename = Hashing.hash(this.originalFilename);
        if(!Strings.isBlank(extension))
        {
            String baseName = FilenameUtils.getBaseName(copy);
            this.filename = Hashing.hash(baseName).concat(".").concat(extension);
        }        
        this.relativePath = Utilities.createPathFromName(this.bucketName).concat(this.bucketName).concat(File.separator).concat(this.filename);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        hash = 23 * hash + Objects.hashCode(this.bucketUuidReference);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaseBucketFile other = (BaseBucketFile) obj;
        return Objects.equals(this.id, other.id);
    }
    
    
}
