package com.mobiweb.bucket.core;

import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.cryptography.Cryptographic;
import java.io.IOException;
import javax.crypto.SecretKey;

/**
 * Defines transformation methods using Cryptographic class instance and a SecretKey object.
 * @author ic
 */
public interface ITransformation
{    
    public byte[] transformed(Cryptographic cryptographic, SecretKey secretKey) throws EncryptionException, IOException;
    
    public void transform(Cryptographic cryptographic, SecretKey secretKey) throws EncryptionException, IOException;
}
