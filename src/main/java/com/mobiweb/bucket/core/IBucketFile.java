/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.core;

/**
 * Defines getters and setters methods on bucket files.
 * @author ic
 */
public interface IBucketFile extends IBucketFileTransform
{    
    public Bucket getBucket();    
    
    public void setBucket(Bucket bucket);    
    
    public String getCreatedAt();    
    
    public void setCreatedAt(String createdAt);    
    
    public String getBucketUuidReference();    
    
    public void setBucketUuidReference(String bucketUuidReference);
    
    public byte[] getContents();
    
    public void setContents(byte[] contents);    
    
    public String getId();
    
    public void setId(String id);
    
    public String getFilename();
    
    public void setFilename(String filename);
    
    public String getOriginalFilename();
    
    public void setOriginalFilename(String originalFilename);
    
    public String getRelativePath();
    
    public void setRelativePath(String relativePath);
    
    public String getBucketName();
    
    public void setBucketName(String bucketName);    
    
    public String getStoragePath();
    
    public void setStoragePath(String storagePath);    
    
}
