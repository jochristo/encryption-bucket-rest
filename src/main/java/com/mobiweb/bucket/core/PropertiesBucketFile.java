package com.mobiweb.bucket.core;

import com.google.gson.annotations.Expose;
import static com.mobiweb.bucket.common.Utilities.createPathFromName;
import com.mobiweb.bucket.common.Hashing;
import com.mobiweb.bucket.cryptography.Cryptographic;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Represents a bucket's property bucket file encrypted under bucket file path.
 * @author ic
 */
public class PropertiesBucketFile extends BaseBucketFile
{      
    
    @Expose
    private Set<RegularBucketFile> containedBucketFiles = new LinkedHashSet<>();     
    
    public Set<RegularBucketFile> getContainedBucketFiles() {
        return containedBucketFiles;
    }

    public void setContainedBucketFiles(Set<RegularBucketFile> containedBucketFiles) {
        this.containedBucketFiles = containedBucketFiles;
    }    

    public PropertiesBucketFile() {
    } 
            
    public PropertiesBucketFile(Bucket bucket)
    {        
        super(bucket, bucket.getPropertiesFile());
        super.setId(Hashing.hash(super.getFilename()));
        //super.setBucket(new Bucket());
        super.setBucket(bucket);
    }

    public PropertiesBucketFile(PropertiesBucketFileItem bucketFileItem, Bucket bucket) {
        super(bucketFileItem, bucket);
        //super.setBucket(new Bucket());
        super.setBucket(bucket);
    }
    
    /*
    public Bucket bucket()
    {
        //Bucket bucket = new Bucket();
        bucket.setUuid(this.getBucketUuidReference());
        bucket.setName(this.getBucketName());
        bucket.setPropertiesFile(this.getFilename());
        bucket.setRelativePath(createPathFromName(this.getBucketName()).concat(this.getBucketName()));  
        Set<RegularBucketFile> set = this.getContainedBucketFiles();
            set.forEach((RegularBucketFile item)->{
                bucket.getBucketFiles().add(item);}); // add item        
        return bucket;
    }
    */
    
    public Bucket bucket()
    {
        //Bucket bucket = new Bucket();
        super.getBucket().setUuid(this.getBucketUuidReference());
        super.getBucket().setName(this.getBucketName());
        super.getBucket().setPropertiesFile(this.getFilename());
        super.getBucket().setRelativePath(createPathFromName(this.getBucketName()).concat(this.getBucketName()));  
        Set<RegularBucketFile> set = this.getContainedBucketFiles();
            set.forEach((RegularBucketFile item)->{
                super.getBucket().getBucketFiles().add(item);}); // add item        
        return super.getBucket();
    }    

    @Override
    public PropertiesBucketFile encrypted(Cryptographic cryptographic) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PropertiesBucketFile decrypted(Cryptographic cryptographic) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
