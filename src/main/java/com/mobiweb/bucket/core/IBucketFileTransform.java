/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.bucket.core;

import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.exceptions.BucketStorageException;
import com.mobiweb.bucket.cryptography.Cryptographic;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Defines transformation methods on IBucketFile objects using Cryptographic class instance.
 * @author ic
 */
public interface IBucketFileTransform
{
    public IBucketFile encrypted(Cryptographic cryptographic) throws BucketStorageException;    
    
    public IBucketFile decrypted(Cryptographic cryptographic) throws EncryptionException, UnsupportedEncodingException, IOException;    
}
