package com.mobiweb.bucket.core;

import com.mobiweb.bucket.cryptography.EncryptionException;
import com.mobiweb.bucket.exceptions.BucketStorageException;
import java.io.IOException;
import javax.crypto.SecretKey;
import javax.ejb.Local;

/**
 * Defines factory and other methods on bucket files.
 * @author ic
 * @param <T>
 * @param <K>
 */
@Local
public interface IBucketFileFactory<T extends IBucket, K extends IBucketFile>
{   
    public K createFile(T bucket, BucketFileItem item, SecretKey secretKey) throws BucketStorageException, EncryptionException;    
    
    public K updateFile(T bucket, BucketFileItem item, SecretKey secretKey) throws BucketStorageException;
    
    public K getBucketFile(T bucket, String filename, SecretKey secretKey) throws BucketStorageException;           
    
    public boolean deleteFile(T bucket, String filename, SecretKey secretKey) throws BucketStorageException, IOException;    
    
    public boolean exists(T bucket, String filename);
}
