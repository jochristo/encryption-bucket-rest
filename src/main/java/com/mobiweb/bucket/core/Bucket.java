package com.mobiweb.bucket.core;

import static com.mobiweb.bucket.common.Utilities.createPathFromName;
import com.mobiweb.bucket.common.Hashing;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Represents a bucket folder in the file system.
 * @author ic
 */
public class Bucket implements IBucket
{
    private String uuid;     
    private String name;    
    private String relativePath;        
    private String propertiesFile;        
    private String absolutePath;
    private Set<BaseBucketFile> bucketFiles = new  LinkedHashSet<>();     
    private static final String PROPERTIES_FILE_EXTENSION = ".properties";      
    
    public Bucket(String name)
    {
        this.name = name;
        this.uuid = Hashing.hash(name);     
        this.propertiesFile = name + PROPERTIES_FILE_EXTENSION;
        this.relativePath = createPathFromName(name).concat(name);
    }

    public Bucket(String name, String serverLocation) {
        this.name = name;        
        this.relativePath = createPathFromName(name).concat(name);        
        this.uuid = Hashing.hash(name);
        this.propertiesFile = name + PROPERTIES_FILE_EXTENSION;
        this.absolutePath = serverLocation + relativePath;
    }  
    
    public Bucket(PropertiesBucketFile propertiesBucketFile)
    {   
        this.uuid = propertiesBucketFile.getBucketUuidReference();
        this.name = propertiesBucketFile.getBucketName();
        this.propertiesFile = propertiesBucketFile.getFilename();
        this.relativePath = createPathFromName(propertiesBucketFile.getBucketName()).concat(propertiesBucketFile.getBucketName());
    }
        
    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getRelativePath() {
        return relativePath;
    }

    @Override
    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }
    
    @Override
    public String getPropertiesFile() {
        return propertiesFile;
    }

    @Override
    public void setPropertiesFile(String propertiesFile) {
        this.propertiesFile = propertiesFile;
    }        

    @Override
    public Set<BaseBucketFile> getBucketFiles() {
        return bucketFiles;
    }

    @Override
    public void setBucketFiles(Set<BaseBucketFile> bucketFiles) {
        this.bucketFiles = bucketFiles;
    }

    @Override
    public String getAbsolutePath() {
        return absolutePath;
    }

    @Override
    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }        

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.uuid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bucket other = (Bucket) obj;
        if (!Objects.equals(this.uuid, other.uuid)) {
            return false;
        }
        return true;
    }
    
}
